$maxTime = 0.5
$adauth = $true
$user = "shoreteluser"
$pass = "shoretelpass"
$server = ""

$Error.Clear()

function Convert-FromUnixdate ($UnixDate) {
   [timezone]::CurrentTimeZone.ToLocalTime(([datetime]'1/1/1970').AddMilliseconds($UnixDate))
}

# Get the current script path
function Get-ScriptPath 
{
    if ($PSScriptRoot) { return $PSScriptRoot } 
    elseif ($psISE) { return split-path -parent $psISE.CurrentFile.Fullpath }
    else { return Split-Path ((Get-Variable MyInvocation -Scope 1).Value).MyCommand.Path }
}
$path = Get-ScriptPath

# if required add it to the module load path to environment variable
if (!($env:PSModulePath -Split ";" -like $path)) { $env:PSModulePath += ";$path" }

# Import the shoretel module
Import-Module ShoreTel -Force -Verbose

if($server -eq "")
{
    $server = Read-Host "Enter shoretel server computer name or ip address"
}

if($adauth)
{
    $session = New-ShoreTel_Session -shoretelServer $server -windowsAuth $true
}
else
{
    $session = New-ShoreTel_Session -shoretelServer $server -windowsAuth $false -user $user -pass $pass
}

if($session.Authenticated)
{
    if($session.LoggedIn)
    {
        $exit = $false
        do
        {
            $null = Start-Sleep -Seconds 5
            $calls = Get-ShoreTel_Calls -session $session
                        
            if($calls)
            {
                Write-Output("Calls: " + $calls.Count)
                foreach($call in $calls)
                {
                    Write-Host("Processing call: " + $call.'call-id')
                    $callStart = Convert-FromUnixdate -UnixDate $calls.state.'timestamp-connect'
                    $callLength = (Get-Date) - $callStart

                    if($callLength.TotalMinutes -gt $maxTime)
                    {
                        Write-Host "Ending call, exceeds maxTime"
                        $null = Stop-ShoreTel_Call -callId $call.'call-id' -session $session
                    }
                }
            }
            else
            {
                Write-Output "No calls"
            }
            if($Error.Count -gt 0)
            {
                $exit = $true
            }
        } until($exit -eq $true)

    }
    else
    {
        Write-Host "Unable to login to shoretel web communicator"
    }
}
else
{
    Write-Host "Unable to authenticate to shoretel"
}