# ShoreTel Communicator PowerShell Modules
  
These PowerShell modules provide a way to interact with the ShoreTel Communicator Web based json api.  
  
There are also some very small modules that provide a way to set presence information on an SA-100 and some very basic ShoreTel Direcotor modules that will probably stop working with the coming ShoreTel Director rework.  
  
Most of this API does not have public documentation so this was mostly built using fiddler to view the recieved JSON messages and responses.  
  
Mainly tested on ShoreTel v13.3 and v14.2  
  
## ShoreTel Communicator PowerShell Modules
ShoreTel\ShoreTel.psm1  
  
### Modules
*New-ShoreTel_Session* - Authenticate and create session object (AD & plain text)  
*Send-ShoreTel_CommunicatorLogin* - Login as a communicator client  
*New-ShoreTel_Call* - Make a call  
*Stop-ShoreTel_Call* - Drop a call  
*Set-ShoreTel_CallHandlingMode* - Set call handling mode  
*Set-ShoreTel_PhoneAssignment* - Assign extension to external number and send extension back home  
*Search-ShoreTel_Contacts* - Search ShoreTel contacts  
*Get-ShoreTel_Calls* - List active calls  
*Get-ShoreTel_VMList* - Get list of voice mails  
*Get-ShoreTel_VMFile* - Download a ShoreTel voice mail as a wave file  
*Send-ShoreTel_PlayVM* - Play a ShoreTel vm  
*Set-ShoreTel_VMSubject* - Set ShoreTel vm subject  
*Move-ShoreTel_VM* - Move ShoreTel vm to a different folder (move to folder 3 to delete)  
*Clear-ShoreTel_DeletedVM* - Clear deleted vm folder  
*Start-ShoreTel_CallRecord* - Record an active call  
*Stop-ShoreTel_CallRecord* - Stop a call recording  
*Get-ShoreTel_CallHistory* - Get call history  
*Send-ShoreTel_CallDigit* - Send digit over a call  
*Set-ShoreTel_WorkgroupLogin* - Login to ShoreTel workgroups  
*Set-ShoreTel_WorkgroupLogout* - Logout of ShoreTel workgroups  
*Set-ShoreTel_WorkgroupEnterWrap* - Enter wrap state in ShoreTel workgroups  

---

## ShoreTel XMPP PowerShell Modules
ShoreTelXMPP\ShoreTelXMPP.psm1  
These modules interact with ShoreTels SA-100 so you can set the XMPP based presence and status notes.  

You must download the agsXMPP.dll from AG Software Solutions links below and copy the dll file into ShoreTelXMPP for these modules to work.  

http://www.ag-software.net/agsxmpp-sdk/  
http://www.ag-software.net/agsxmpp-sdk/download/  

### Modules
*New-XMPP_Session* - Authenticate and create XMPP session object, 
agsXMPP doesn't support kerberos so no windows authentication pass-through  
*Set-XMPP_Presence* - Set your presence status and status note  

---

## ShoreTel Director PowerShell Modules
ShoreTelDirector\ShoreTelDirector.psm1  

### Modules
*Remove-ShoreTelDirectorUser* - Remove a user from the ShoreTel Director  
*Get-ShoreTelDirectorUser* - Get details about a user from ShoreTel Director  
