# Get the current script path
function Get-ScriptPath 
{
    if ($PSScriptRoot) { return $PSScriptRoot } 
    elseif ($psISE) { return split-path -parent $psISE.CurrentFile.Fullpath }
    else { return Split-Path ((Get-Variable MyInvocation -Scope 1).Value).MyCommand.Path }
}
$path = Get-ScriptPath

# if required add it to the module load path to environment variable
if (!($env:PSModulePath -Split ";" -like $path)) { $env:PSModulePath += ";$path" }

# Import the shoretel module
Import-Module ShoreTelXMPP -Force -Verbose


Write-Host("You must download and place agsXMPP.dll in the directory " + $path + "\ShoreTelXMPP\ for this script to work or manually the the path variable") -ForegroundColor Green
Import-XMPP_AGSXMPPDll -path ($path + "\ShoreTelXMPP\agsXMPP.dll")

$session = New-XMPP_Session -server (Read-Host -Prompt "Server") -user (Read-Host -Prompt "Username") -pass (Read-Host -Prompt "Password") -domain (Read-Host -Prompt "IM Domain")

if($session.Authenticated)
{
    Write-Host "Setting status to busy"
    Set-XMPP_Presence -presence Busy -statusNote "I am rather busy" -session $session 
}