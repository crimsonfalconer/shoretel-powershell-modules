

function New-Param
{
    param
    (
        $data
    )

    return  "<param><![CDATA[" + $data + "]]></param>"
}

function New-SOAPEnvelope
{
    param
    (
        $SOAPAction,
        $params
    )
    # Bruteforce Create XML
    $xml = "<?xml version=`"1.0`" encoding=`"utf-8`"?><soap:Envelope xmlns:soap=`"http://schemas.xmlsoap.org/soap/envelope/`"><soap:Body>"
    $xml += "<"  + $soapAction +">"
    $xml += $params
    $xml += "</"  + $soapAction +">"
    $xml += "</soap:Body></soap:Envelope>"
    return $xml
}

function New-WC
{
  <#
  .SYNOPSIS
  Returns a new webclient object
  .DESCRIPTION
  Returns a new webclient object, 
  with the header "Content-Type", "text/xml; charset=utf-8" added
  set to use the default windows credentials
  .EXAMPLE
  $webclient = New-WC
  #>
    param
    (
        [switch] $IgnoreSSL,
        [switch] $CookieAware,
        [bool] $UseDefaultCredentials = $true,
        $headers
    )

    if($IgnoreSSL) {[System.Net.ServicePointManager]::ServerCertificateValidationCallback = {$true}}

    if($CookieAware)
    {
$CookieAwareWebClient = @"
public class CookieAwareWebClient : System.Net.WebClient
{
  public CookieAwareWebClient()
  {
    System.Net.ServicePointManager.Expect100Continue = true;
  }

  private System.Net.CookieContainer m_container = new System.Net.CookieContainer();
  protected override System.Net.WebRequest GetWebRequest(System.Uri address)
  {
    System.Net.WebRequest request = base.GetWebRequest(address);
    if (request is System.Net.HttpWebRequest)
    {
      (request as System.Net.HttpWebRequest).CookieContainer = m_container;
    }
    return request;
  }
}
"@
        Add-Type -TypeDefinition $CookieAwareWebClient
        $webClient = New-Object CookieAwareWebClient
        foreach($header in $headers)
        {
          $webClient.Headers.add($header.Name, $header.Value)
        }
    }
    else
    {
        $webClient = New-Object System.Net.WebClient
        foreach($header in $headers)
        {
          $webClient.Headers.add($header.Name, $header.Value)
        }
    }

    $webClient.UseDefaultCredentials = $UseDefaultCredentials
    return $webClient
}

function Send-Web_Post
{
    param (
        [string] $URL, 
        [string] $postData, 
        [object] $webClient
    )
    Write-Debug $URL
    Write-Debug $postData

    $res = $webClient.UploadString($URL, $postData)
    if($res) { return $res }
    else { return $false }
}

function Send-ShoreTelDirectorLogin
{
    param 
    (
        [string] $shoretelServer,
        [bool] $windowsAuth,
        [string] $user,
        [string] $pass
    )

    if($shoretelServer -eq ""){Throw "You must provide a ShoreTel server ip or hostname" }
    if($windowsAuth -eq $false -and ($user -eq "" -or $pass -eq "")){ Throw "You must provide a username or password if not using windows authentication" }

    $session = @{}
    $session.Server = $shoretelServer
    $session.SessionType = "Director"
    $session.Authenticated = $false
    $session.SequenceId = 1
    $session.webClient = New-WC -windowsAuth $windowsAuth -CookieAware

    if($windowsAuth)
    {
        $queryString = "?doingSSO=1"
        $response = Get-Web -url ("http://" + $shoretelServer + "/shorewaredirector/validate.asp" + $queryString) -webClient $session.webClient
    }
    else
    {   
        $session.webClient.Headers.Add("Content-Type", "application/x-www-form-urlencoded")
        $md5Pass = ConvertTo-MD5 -string $pass
        $postData = "hpassword=" + $md5Pass
        $postData += "&login=" + $user
        $postData += "&password="
        $postData += "&SUBMIT1=Login"
        $response = Send-Web_Post -url ("http://" + $shoretelServer + "/shorewaredirector/validate.asp") -postData $postData -webClient $session.webClient
    }
    if($response.StartsWith("<script> location.href='mainframe.asp'</script>"))
    {
        $session.Authenticated = $true
    }
    return $session
}

function ConvertTo-MD5($string) {
    $md5 = new-object -TypeName System.Security.Cryptography.MD5CryptoServiceProvider
    $utf8 = new-object -TypeName System.Text.UTF8Encoding
    $hash = [System.BitConverter]::ToString($md5.ComputeHash($utf8.GetBytes($String))).Replace("-", "")
    return $hash.ToLower()
}

function Remove-ShoreTelDirectorUser
{
    param 
    (
        [string] $extension,
        [switch] $returnResults = $false,
        [object] $session
    )
    if($session.SessionType -ne "Director"){ Throw "Invalid session object" }
    if(!($session.Authenticated)){ Throw "Session not authenticated" }

    $user = Get-ShoreTelDirectorUser -extension $extension -session $session -ErrorAction SilentlyContinue
    if(!($user)){ throw "Extension does not exist" }

    $url = "http://" + $session.Server + "/shorewaredirector/user_actions.asp"
    $postData = New-SOAPEnvelope -SOAPAction "Delete" -params (New-Param -data $extension)

    Write-Debug("Posting XML: " + $postData)
    Write-Debug("URL: " + $url)

    $response = Send-Web_Post -URL $url -postData $postData -webClient $session.webClient -verbose $PSBoundParameters['Verbose']

    if($returnResults) 
    {
        return $response
    }
    else
    {
        $xml = [xml]$response
        if($xml.Envelope.Body.message.value.'#cdata-section' -eq 1)
        {
            return ("User with extension " + $extension + " deleted.")
        }
        else
        {
            Write-Error $xml.Envelope.Body.message.value.'#cdata-section'
            return
        }

        Write-Debug $response
        if($response.response -gt -1) {return $true} else {return $false}
    }
}

function Get-ShoreTelDirectorUser
{
    [CmdletBinding()]
    param 
    (
        [string] $extension,
        [switch] $returnResults,
        [object] $session
    )
    if($session.SessionType -ne "Director"){ Throw "Invalid session object" }
    if(!($session.Authenticated)){ Throw "Session not authenticated" }

    $url = "http://" + $session.Server + "/shorewaredirector/user_actions.asp"
    $postData = New-SOAPEnvelope -SOAPAction "Get" -params (New-Param -data $extension)

    Write-Debug("Posting XML: " + $postData)
    Write-Debug("URL: " + $url)

    $response = Send-Web_Post -URL $url -postData $postData -webClient $session.webClient -verbose $PSBoundParameters['Verbose']

    if($returnResults)
    {
        return $response
    }
    else
    {
        $xml = [xml]$response
        if($xml.Envelope.Body.message.value.'#cdata-section' -eq 0)
        {
            if(!($ignoreError)){Write-Error "Failed to retrieve user"}
            return
        }
        $objUser = New-Object -TypeName PSObject
        foreach($node in $xml.Envelope.Body.message.object.ChildNodes)
        {
            if($node.LocalName -ne 'SO1')
            {
                $objUser | Add-Member -MemberType NoteProperty -Name $node.LocalName -Value $node.'#cdata-section'
            }
        }
        return $objUser
    }
}

function New-ShoreTelDirectorDirectoryEntry
{
    [CmdletBinding()]
    param 
    (
        [string] $firstName,
        [string] $lastName,
        [string] $homePhone,
        [string] $workPhone,
        [string] $faxPhone,
        [string] $cellPhone,
        [string] $pagerPhone,
        [string] $email,
        [switch] $returnResults,
        [object] $session
    )
    if($session.SessionType -ne "Director"){ Throw "Invalid session object" }
    if(!($session.Authenticated)){ Throw "Session not authenticated" }

    $url = "http://" + $session.Server + "/shorewaredirector/tabaddress_actions.asp"

    $params = New-Param -data -1
    $params += New-Param -data $firstName
    $params += New-Param -data $lastName
    $params += New-Param -data $homePhone
    $params += New-Param -data $workPhone
    $params += New-Param -data $faxPhone
    $params += New-Param -data $cellPhone
    $params += New-Param -data $pagerPhone
    $params += New-Param -data $email
    $params += New-Param -data ""

    $postData = New-SOAPEnvelope -SOAPAction "Update" -params $params

    Write-Debug("Posting XML: " + $postData)
    Write-Debug("URL: " + $url)

    $response = Send-Web_Post -URL $url -postData $postData -webClient $session.webClient -verbose $PSBoundParameters['Verbose']

    if($returnResults)
    {
        return $response
    }
    else
    {
        $xml = [xml]$response
        if($xml.Envelope.Body.message.value.type -eq 'number')
        {
            return $xml.Envelope.Body.message.value.'#cdata-section'
        }
        else
        {
            Write-Error $xml.Envelope.Body.message.value.'#cdata-section'
            return
        }
    }
}

function Import-ShoreTelDirectorDirectoryEntryFromCSV
{
    [CmdletBinding()]
    param
    (
        [string] $csvPath,
        $delimiter = ",",
        [object] $session
    )
    if($session.SessionType -ne "Director"){ Throw "Invalid session object" }
    if(!($session.Authenticated)){ Throw "Session not authenticated" }

    $csv = Import-Csv -Path $csvPath -Delimiter $delimiter
    $responses = @()
    foreach($field in $csv)
    {
        Write-Verbose("Creating directory entry - " + $field)
        $responses += New-ShoreTelDirectorDirectoryEntry -firstName $field.firstName -lastName $field.lastName -homePhone $field.homePhone -workPhone $field.workPhone -faxPhone $field.faxPhone -cellPhone $field.cellPhone -pagerPhone $field.pagerPhone -email $field.email -session $session
    }
    return $responses
}