param
(
    $folder = (Read-Host "What folder to download the voice mails to?, Default: C:\Temp\"),
    $adauth = $true,
    $user = "shoreteluser",
    $pass = "shoretelpass",
    $server = ""
)

if($folder -eq '')
{
    $folder = "C:\Temp\"
}

$Error.Clear()

# Get the current script path
function Get-ScriptPath 
{
    if ($PSScriptRoot) { return $PSScriptRoot } 
    elseif ($psISE) { return split-path -parent $psISE.CurrentFile.Fullpath }
    else { return Split-Path ((Get-Variable MyInvocation -Scope 1).Value).MyCommand.Path }
}
$path = Get-ScriptPath

# if required add it to the module load path to environment variable
if (!($env:PSModulePath -Split ";" -like $path)) { $env:PSModulePath += ";$path" }

# Import the shoretel module
Import-Module ShoreTel -Force -Verbose

if($server -eq "")
{
    $server = Read-Host "Enter shoretel server computer name or ip address"
}

if($adauth)
{
    $session = New-ShoreTel_Session -shoretelServer $server -windowsAuth $true
}
else
{
    $session = New-ShoreTel_Session -shoretelServer $server -windowsAuth $false -user $user -pass $pass
}

if($session.Authenticated)
{
    if($session.LoggedIn)
    {
        $vms = Get-ShoreTel_VMList -session $session
        
        foreach($vm in $vms)
        {
            $vmId = $vm."msg-id"
            $time = Convert-FromUnixdate -UnixDate $vm.'rcvd-timestamp'
            Get-ShoreTel_VMFile -mailboxId $session.UserDN -msgId $vmId -session $session -file ($folder + $time.ToString("yyyyMMddhhmm") + " " + $vm.'sender-name' + " " + $vm.'sender-number' + " " + $vmId + ".wav")
        }
    }
    else
    {
        Write-Host "Unable to login to shoretel web communicator"
    }
}
else
{
    Write-Host "Unable to authenticate to shoretel"
}