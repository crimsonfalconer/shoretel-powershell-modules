param
(
    #$state = (Read-Host "`r`n1. Login `r`n2. Logout `r`n3. Wrap-up `r`n"),
    $adauth = $true,
    #$user = "shoreteluser",
    #$pass = "shoretelpass",
    $server = "x.x.x.x"
)

$Error.Clear()

# Get the current script path
function Get-ScriptPath 
{
    if ($PSScriptRoot) { return $PSScriptRoot } 
    elseif ($psISE) { return split-path -parent $psISE.CurrentFile.Fullpath }
    else { return Split-Path ((Get-Variable MyInvocation -Scope 1).Value).MyCommand.Path }
}
$path = Get-ScriptPath

# if required add it to the module load path to environment variable
if (!($env:PSModulePath -Split ";" -like $path)) { $env:PSModulePath += ";$path" }

# Import the shoretel module
Import-Module ShoreTel -Force -Verbose

if($server -eq "")
{
    $server = Read-Host "Enter shoretel server computer name or ip address"
}

# Create session
$session = New-ShoreTel_Session -shoretelServer $server -windowsAuth $adauth -user $user -pass $pass

ShoreTel-SubscribeEvents -session $session

$loop = $true
do {
    $events = Get-Events -session $session
    foreach($event in $events)
    {
        $event.call.state.state
        if($event.call.state.state -eq 18 -and $event.call.info."caller-num" -eq "Dialler")
        {
            
            Start-Sleep -Milliseconds 400
            Invoke-ShoreTel_CallPickup -callId $event.call.'call-id' -session $session
        }
    }
    Start-Sleep 3
} while($loop)
