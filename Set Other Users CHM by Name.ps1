param
(
    $CHM = (Read-Host "`r`n1. Normal `r`n2. In a Meeting `r`n3. Out of Office `r`n4. Extendend Absence `r`n5. Custom`r`n"),
    $otherUser = (Read-Host "User Id"),
    $adauth = $true,
    $user = "shoreteluser",
    $pass = "shoretelpass",
    $server = "",
    $allowMultiple = $false
)

# Get the current script path
function Get-ScriptPath 
{
    if ($PSScriptRoot) { return $PSScriptRoot } 
    elseif ($psISE) { return split-path -parent $psISE.CurrentFile.Fullpath }
    else { return Split-Path ((Get-Variable MyInvocation -Scope 1).Value).MyCommand.Path }
}
$path = Get-ScriptPath

# if required add it to the module load path to environment variable
if (!($env:PSModulePath -Split ";" -like $path)) { $env:PSModulePath += ";$path" }

# Import the shoretel module
Import-Module ShoreTel -Force

if($server -eq ""){ $server = Read-Host "Enter shoretel server computer name or ip address" }

# Create New ShoreTel Session
if($adauth){ $session = New-ShoreTel_Session -shoretelServer $server -windowsAuth $true }
else{ $session = New-ShoreTel_Session -shoretelServer $server -windowsAuth $false -user $user -pass $pass }

# Search for user
$results = Search-ShoreTel_Contacts -searchFor $otherUser -session $session

# Error if multiple results not allowed
if($results.Count -gt 1 -and $allowMultiple -eq $false)
{
    Write-Error "Multiple results not allowed"
    exit 1
}

# Set their call handling mode
foreach($contact in $results)
{
    $cp = $contact.'contact-points' | Where-Object { $_.type -eq 16}
    Set-ShoreTel_CallHandlingMode -CHM $CHM -ext $cp.addr -session $session
}