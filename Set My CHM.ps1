param
(
    $CHM = (Read-Host "`r`n1. Normal `r`n2. In a Meeting `r`n3. Out of Office `r`n4. Extendend Absence `r`n5. Custom`r`n"),
    $adauth = $true,
    $user = "shoreteluser",
    $pass = "shoretelpass",
    $server = ""
)

$Error.Clear()

# Get the current script path
function Get-ScriptPath 
{
    if ($PSScriptRoot) { return $PSScriptRoot } 
    elseif ($psISE) { return split-path -parent $psISE.CurrentFile.Fullpath }
    else { return Split-Path ((Get-Variable MyInvocation -Scope 1).Value).MyCommand.Path }
}
$path = Get-ScriptPath

# if required add it to the module load path to environment variable
if (!($env:PSModulePath -Split ";" -like $path)) { $env:PSModulePath += ";$path" }

# Import the shoretel module
Import-Module ShoreTel -Force -Verbose

if($server -eq "")
{
    $server = Read-Host "Enter shoretel server computer name or ip address"
}

Set-ShoreTel_CallHandlingMode -CHM $CHM -shoretelServer $server -windowsAuth $adauth -user $user -pass $pass