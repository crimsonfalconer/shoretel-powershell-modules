﻿# Get the current script path
function Get-ScriptPath 
{
    if ($PSScriptRoot) { return $PSScriptRoot } 
    elseif ($psISE) { return split-path -parent $psISE.CurrentFile.Fullpath }
    else { return Split-Path ((Get-Variable MyInvocation -Scope 1).Value).MyCommand.Path }
}
$path = Get-ScriptPath

# if required add it to the module load path to environment variable
if (!($env:PSModulePath -Split ";" -like $path)) { $env:PSModulePath += ";$path" }

# Import the shoretel module
Import-Module ShoreTelDirector -Force -Verbose

$session = Send-ShoreTelDirectorLogin -shoretelServer "x.x.x.x" -windowsAuth $true
#$session = Send-ShoreTelDirectorLogin -shoretelServer "x.x.x.x" -windowsAuth $false -user "myuser" -pass "password"
$session.Authenticated
$firstName = "Daffy"
$lastName = "Duck"
$homePhone = "+61000000001"
$workPhone = "+61000000002"
$faxPhone = "+61000000003"
$cellPhone = "+61000000004"
$pagerPhone = "+61000000005"
$email = "test@test.com"

Write-Host("Firstname = " + $firstName)
Write-Host("Surname = " + $lastName)
Write-Host("Home Phone = " + $homePhone)
Write-Host("Work Phone = " + $workPhone)
Write-Host("Fax = " + $faxPhone)
Write-Host("Mobile Phone = " + $cellPhone)
Write-Host("Pager = " + $pagerPhone)
Write-Host("Email = " + $email)

$confirm = Read-Host "Create new directory entry with the above details?"

if($confirm -eq 'Y')
{
    $results = New-ShoreTelDirectorDirectoryEntry -firstName $firstName -lastName $lastName -homePhone $homePhone -workPhone $workPhone -faxPhone $faxPhone -cellPhone $cellPhone -pagerPhone $pagerPhone -email $email -session $session -Verbose
    $results
}