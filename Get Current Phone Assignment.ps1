param
(
    $adauth = $true,
    $user = "shoreteluser",
    $pass = "shoretelpass",
    $server = ""
)

$Error.Clear()

# Get the current script path
function Get-ScriptPath 
{
    if ($PSScriptRoot) { return $PSScriptRoot } 
    elseif ($psISE) { return split-path -parent $psISE.CurrentFile.Fullpath }
    else { return Split-Path ((Get-Variable MyInvocation -Scope 1).Value).MyCommand.Path }
}
$path = Get-ScriptPath

# if required add it to the module load path to environment variable
if (!($env:PSModulePath -Split ";" -like $path)) { $env:PSModulePath += ";$path" }

# Import the shoretel module
Import-Module ShoreTel -Force -Verbose

if($server -eq "")
{
    $server = Read-Host "Enter shoretel server computer name or ip address"
}

$props = Get-ShoreTel_ExtensionProperties -shoretelServer $server -windowsAuth $adauth -user $user -pass $pass
$props
$assignment = $props."ext-props"."phone-assignment"
Write-Host ("Phone Assignment: " + $assignment)
switch($assignment)
{
    1 { Write-Host "Primary Phone" }
    2 { Write-Host "Softphone" }
    3 { Write-Host "External Assignment" }
    default {Write-Host "?" }
}