param
(
    $ext = (Read-Host "Enter agent extension"),
    $state = (Read-Host "`r`n1. Login `r`n2. Logout `r`n3. Wrap-up `r`n"),
    $adauth = $true,
    $user = "shoreteluser",
    $pass = "shoretelpass",
    $server = ""
)

$Error.Clear()

# Get the current script path
function Get-ScriptPath 
{
    if ($PSScriptRoot) { return $PSScriptRoot } 
    elseif ($psISE) { return split-path -parent $psISE.CurrentFile.Fullpath }
    else { return Split-Path ((Get-Variable MyInvocation -Scope 1).Value).MyCommand.Path }
}
$path = Get-ScriptPath

# if required add it to the module load path to environment variable
if (!($env:PSModulePath -Split ";" -like $path)) { $env:PSModulePath += ";$path" }

# Import the shoretel module
Import-Module ShoreTel -Force -Verbose

if($server -eq "")
{
    $server = Read-Host "Enter shoretel server computer name or ip address"
}

if($adauth)
{
    $session = New-ShoreTel_Session -shoretelServer $server -windowsAuth $true
}
else
{
    $session = New-ShoreTel_Session -shoretelServer $server -windowsAuth $false -user $user -pass $pass
}

if($session.Authenticated)
{
    if($session.LoggedIn)
    {
        $agents = Get-ShoreTel_WorkgroupAgents -session $session

        foreach($agent in $agents)
        {
            if($agent."agent-ext" -eq $ext)
            {
                $foundAgent = $true
                Write-Host "AgentId found setting workgroup status."
                if($state -eq "1")
                {
                    Set-ShoreTel_WorkgroupLogin -session $session -agentId $agent.'agent-id'
                }
                elseif($state -eq "2")
                {
                    Set-ShoreTel_WorkgroupLogout -session $session -agentId $agent.'agent-id'
                }
                elseif($state -eq "3")
                {
                    Set-ShoreTel_WorkgroupEnterWrap -session $session -agentId $agent.'agent-id'
                }
            }
        }

        if($foundAgent -eq $false){Write-Host "Unable to find agent with that extension number."}
    }
    else
    {
        Write-Host "Unable to login to shoretel web communicator"
    }
}
else
{
    Write-Host "Unable to authenticate to shoretel"
}