# Get the current script path
function Get-ScriptPath 
{
    if ($PSScriptRoot) { return $PSScriptRoot } 
    elseif ($psISE) { return split-path -parent $psISE.CurrentFile.Fullpath }
    else { return Split-Path ((Get-Variable MyInvocation -Scope 1).Value).MyCommand.Path }
}
$path = Get-ScriptPath

# if required add it to the module load path to environment variable
if (!($env:PSModulePath -Split ";" -like $path)) { $env:PSModulePath += ";$path" }

# Import the shoretel module
Import-Module ShoreTel -Force -Verbose

$server = Read-Host "Enter shoretel server computer name or ip address"
$adauth = Read-Host "Use AD authentication? (Y)"

if($adauth -eq 'y' -or $adauth -eq 'yes')
{
    $session = New-ShoreTel_Session -shoretelServer $server -windowsAuth $true
}
else
{
    $user = Read-Host "Username"
    $pass = Read-Host "Password"
    $session = New-ShoreTel_Session -shoretelServer $server -windowsAuth $false -user $user -pass $pass
}

if($session.Authenticated)
{
    if($session.LoggedIn)
    {
        Clear-Host
        $exit = $false
        do {
            Write-Host "THIS CODE HAS BEEN CREATED FOR EXAMPLES ONLY, you will have to modify it for use actively in a production environment.`r`n USE THIS CODE AND THE ATTACHED MODULE AT YOUR OWN RISK.`r`n`r`n`r`n"
            Write-Host "1. Set call handling mode"
            Write-Host "2. Make call"
            Write-Host "3. Drop call"
            Write-Host "4. Display current call list"
            Write-Host "5. Display 10 history records"
            Write-Host "6. Display Voice message list"
            Write-Host "7. Set external phone assignment"
            Write-Host "8. Search Contacts"
            Write-Host "9. Record call"
            Write-Host "10. Delete a voice mail"
            Write-Host "0. Exit"

            $in = Read-Host
            switch($in)
                {

                    # Set Call Handling Mode
                    1 { 
                        $CHM = Read-Host "`r`n1. Normal `r`n2. In a Meeting `r`n3. Out of Office `r`n4. Extendend Absence `r`n5. Custom`r`n"
                        Set-ShoreTel_CallHandlingMode -session $session -CHM $CHM
                      }
                    # Make Call
                    2 {
                        $phoneNumber = Read-Host "Enter number to call"
                        New-ShoreTel_Call -session $session -phoneNumber $phoneNumber
                      }
                    # Drop Call
                    3 {
                        $calls = Get-ShoreTel_Calls -session $session
                        if($calls)
                        {
                            Write-Host "`r`nCall list`r`n------------"
                            $x = 0
                            foreach($call in $calls)
                            {
                                Write-Host ("" + $x + ". " + $calls[$x]."call-id")
                                $x++
                            }
                            $callToDrop = Read-Host "Enter the number of the call to drop, blank to cancel"
                            if($callToDrop -ne '') {Stop-ShoreTel_Call -session $session -callId $calls[$callToDrop]."call-id"}
                        }
                      }
                    # Display current call list
                    4 {
                        $calls = Get-ShoreTel_Calls -session $session
                        if($calls){$calls}
                      }
                    # Display 10 history records
                    5 {
                        $hist = Get-ShoreTel_CallHistory -PageSize 10 -session $session
                        if($hist){$hist.records}
                      }
                    # Display voice mail list
                    6 {
                        $vms = Get-ShoreTel_VMList -session $session
                        $vms  
                      }
                    # Set external phone assignment
                    7 {
                        $extAssign = Read-Host "Enter phone number for external assignment, leave blank to send home"
                        if($extAssign -ne '')
                        {
                            Set-ShoreTel_PhoneAssignment -session $session -externalNumber $extAssign
                        }
                        else
                        {
                            Set-ShoreTel_PhoneAssignment -session $session -sendHome
                        }
                      }
                    # Search contacts
                    8 {
                        $search = Read-Host "Search for"
                        $results = Search-ShoreTel_Contacts -searchFor $search -session $session
                        foreach($contact in $results)
                        {
                            Write-Host($contact.first + " " + $contact.last + " : " + $contact."source-name")
                        }
                      }
                    # Record active call
                    9 {
                        $calls = Get-ShoreTel_Calls -session $session
                        if($calls)
                        {
                            Write-Host "`r`nCall list`r`n------------"
                            $x = 0
                            foreach($call in $calls)
                            {
                                Write-Host ("" + $x + ". " + $calls[$x]."call-id")
                                $x++
                            }
                            $callNo = Read-Host "Enter the number of the call to record, blank to cancel"
                            if($callNo -ne '') {Start-ShoreTel_CallRecord -session $session -callId $calls[$callNo]."call-id" -ext $session.UserDN} 
                        }
                      }
                    # Delete a voice mail
                    10 {
                        $vms = Get-ShoreTel_VMList -session $session
                        if($vms)
                        {
                            Write-Host "`r`nVM list`r`n------------"
                            $x = 0
                            foreach($vm in $vms)
                            {
                                Write-Host ("" + $x + ". " + $vms[$x]."msg-id" + "   " + $vms[$x]."sender-name" + "   " + $vms[$x]."sender-number")
                                $x++
                            }
                            $vmNo = Read-Host "Enter the number of the vm to delete, blank to cancel"
                            if($vmNo -ne '') {Move-ShoreTel_VM -session $session -msgIds $vms[$vmNo]."msg-id" -vmFolder 3} 
                        }
                      }
                    0 {$exit = $true}
                }
        } until($exit -eq $true)

    }
    else
    {
        Write-Host "Unable to login to shoretel web communicator"
    }
}
else
{
    Write-Host "Unable to authenticate to shoretel"
}