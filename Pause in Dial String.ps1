param
(
    $adauth = $true,
    $user = "shoreteluser",
    $pass = "shoretelpass",
    $server = ""
)

$Error.Clear()

# Get the current script path
function Get-ScriptPath 
{
    if ($PSScriptRoot) { return $PSScriptRoot } 
    elseif ($psISE) { return split-path -parent $psISE.CurrentFile.Fullpath }
    else { return Split-Path ((Get-Variable MyInvocation -Scope 1).Value).MyCommand.Path }
}
$path = Get-ScriptPath

# if required add it to the module load path to environment variable
if (!($env:PSModulePath -Split ";" -like $path)) { $env:PSModulePath += ";$path" }

# Import the shoretel module
Import-Module ShoreTel -Force -Verbose

if($server -eq "")
{
    $server = Read-Host "Enter shoretel server computer name or ip address"
}

if($adauth)
{
    $session = New-ShoreTel_Session -shoretelServer $server -windowsAuth $true
}
else
{
    $session = New-ShoreTel_Session -shoretelServer $server -windowsAuth $false -user $user -pass $pass
}

if($session.Authenticated)
{
    if($session.LoggedIn)
    {
        $callSuccess = New-ShoreTel_Call -session $session -phoneNumber "0249355000"
        
        if($callSuccess -eq $true)
        {
            Start-Sleep 3
            $calls = Get-ShoreTel_Calls -session $session
            $call = $calls[0]."call-id"
            Send-ShoreTel_CallDigit -session $session -callId $call -digits "1"
            Start-Sleep 3
            Send-ShoreTel_CallDigit -session $session -callId $call -digits "1281"
        }
    }
    else
    {
        Write-Host "Unable to login to shoretel web communicator"
    }
}
else
{
    Write-Host "Unable to authenticate to shoretel"
}