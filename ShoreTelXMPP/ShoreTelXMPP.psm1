

function Import-XMPP_AGSXMPPDll
{
    <#
    .SYNOPSIS
    Loads the AGSXMPP.dll Type information.
    .DESCRIPTION
    Loads the AGSXMPP.dll Type information and sets a global variable to say they've been loaded.
    .EXAMPLE
    Load-XMPP_AGSXMPPDll -path "C:\AgsXMPP\agsXMPP.dll"
    .PARAMETER path
    The path to the agsXMPP.dll file.
    #>
    param 
    (
       [string] $path = ((Get-Location).Path + "\agsXMPP.dll")
    )

    Write-Host("Loading XMPP Dll - " + $agsXMPPDll)

    try
    {
        if(Test-Path $agsXMPPDll)
        {
            Add-Type -LiteralPath $agsXMPPDll
        }
        else
        {
            Write-Error "Unable to find agsXMPP.dll please place in working directory or provide path when loading modules"
        }
        Write-Host("Successfully loaded agsXMPP.dll")
        $Global:agsXMPPLoaded = $true
    }
    catch
    {
        $Global:agsXMPPLoaded = $false
        throw $_
    }
}

function New-XMPP_Session
{
    <#
    .SYNOPSIS
    Creates a new XMPP Session object
    .DESCRIPTION
    Creates a new XMPP session object and authenticates you against the XMPP server.
    .EXAMPLE
    New-XMPP_Session
    .EXAMPLE
    Set-Presence -presence Online | $session 
    .EXAMPLE
    Set-Presence -presence Online -server {ip/host} -user {user} -pass {pass} -domain {IM Domain}
    .PARAMETER server
    The server or hostname of the XMPP Server, Your SA-100 not your ShoreTel server.
    .PARAMETER user
    ShoreTel username
    .PARAMETER pass
    Your ShoreTel password in a windows environment this would be your windows password. 
    .PARAMETER domain
    The IM domain defined in ShoreTel Director. ShoreTel Director -> Administration -> System Parameters -> Other
    .PARAMETER tlsEnabled
    Wether or not to use TLS defined in ShoreTel Director. ShoreTel Director -> Administration -> System Parameters -> Other
    #>
    param
    (
        [Parameter(Mandatory=$true)]
        [string] $server,
        [Parameter(Mandatory=$true)]
        [string] $user,
        [Parameter(Mandatory=$true)]
        [string] $pass,
        [string] $domain,
        
        [bool] $tlsEnabled = $false
    )
    if($Global:agsXMPPLoaded -eq $false){Write-Error "AGSXMPP Dll Types are not loaded"; return}

    $session = @{}
    $session.JID = $user + "@" + $domain
    $session.User = $user
    $session.Xmpp = New-Object agsXMPP.XmppClientConnection($server)
    if($tls){$session.Xmpp.UseStartTLS = $true}

    $job = Register-ObjectEvent -InputObject $session.Xmpp -EventName "OnLogin" -SourceIdentifier "XMPPLogin" -Action ({
        Write-Output "LoggedIn"
    })

    $session.Xmpp.Open($user, $pass)

    Wait-Event "XMPPLogin" -Timeout 10
    $session.Results = Receive-Job $job

    if($job.State -eq "NotStarted")
    {
        Write-Error "Timeout"
        $session.Error = "Timeout"
    }

    if($session.Results = "LoggedIn" -and $session.Xmpp.Authenticated -eq $true)
    {
        Write-Host "Login Success"
        $session.Error = ""
        $session.Authenticated = $true
    }

    Unregister-Event -SourceIdentifier "XMPPLogin"
    return $session
}

function New-XMPP_PresenceStatusXML
{
    param(
        $show,
        $status = "",
        $activity = "None",
        $user
    )
    $xml = '<presence><show>{0}</show><status>{1}</status><priority>0</priority><EnhancedPresence xmlns="ext:ShoreTelEnhancedPresence"><category><state user="{2}" device="Active" activity="{3}"></state></category></EnhancedPresence></presence>'
    return [string]::Format($xml, $show, $status, $user, $activity)
}

function Set-XMPP_Presence
{
    <#
    .SYNOPSIS
    Set your current presence.
    .DESCRIPTION
    Sends an XML message that will change your presnce status.
    .EXAMPLE
    Set-Presence -presence Busy -statusNote "I am rather busy" -session $session 
    .EXAMPLE
    Set-Presence -presence Online | $session 
    .EXAMPLE
    Set-Presence -presence Online -server {ip/host} -user {user} -pass {pass} -domain {IM Domain}
    .PARAMETER presence
    Presence type to set your current presence status too
    .PARAMETER statusNote
    Status note to set
    .PARAMETER session
    The XMPP session object
    #>
    param
    (
        [ValidateSet("AppearOffline", "Online", "Busy", "InAMeeting", "Away", "BeRightBack", "DoNotDisturb")]
        [string] $presence,
        
        [string] $statusNote,

        [Parameter(ValueFromPipeline=$true)] 
        [object] $session,
        
        [string] $server,
        [string] $user,
        [string] $pass,
        [string] $domain 
    )
    if($server -ne ""){$session = New-XMPP_Session -server $server -user $user -pass $pass -domain $domain}
    elseif($session -eq $null){Write-Error "Session object must be provided"; return}
    if($session.XMPP.Authenticated -eq $false){Write-Error "Session must be authenticated"; return}

    $presenceXML = ""

    switch($presence)
    {
        "AppearOffline" {$presenceXML = New-XMPP_PresenceStatusXML -show "unavailable" -user "Offline" -status $statusNote}
        "Online" {$presenceXML = New-XMPP_PresenceStatusXML -show "chat" -user "Available" -status $statusNote}
        "Busy" {$presenceXML = New-XMPP_PresenceStatusXML -show "dnd" -user "Busy" -status $statusNote}
        "InAMeeting" {$presenceXML = New-XMPP_PresenceStatusXML -show "away" -user "Busy" -activity "Meeting" -status $statusNote}
        "Away" {$presenceXML = New-XMPP_PresenceStatusXML -show "away" -user "Away" -status $statusNote}
        "BeRightBack" {$presenceXML = New-XMPP_PresenceStatusXML -show "away" -user "Be Right Back" -status $statusNote}
        "DoNotDisturb" {$presenceXML = New-XMPP_PresenceStatusXML -show "dnd" -user "DND" -status $statusNote}
    }
    Write-Debug("Sending presence message xml: " + $presenceXML)
    $session.XMPP.Send($presenceXML)
}
