﻿# Get the current script path
function Get-ScriptPath 
{
    if ($PSScriptRoot) { return $PSScriptRoot } 
    elseif ($psISE) { return split-path -parent $psISE.CurrentFile.Fullpath }
    else { return Split-Path ((Get-Variable MyInvocation -Scope 1).Value).MyCommand.Path }
}
$path = Get-ScriptPath

# if required add it to the module load path to environment variable
if (!($env:PSModulePath -Split ";" -like $path)) { $env:PSModulePath += ";$path" }

# Import the shoretel module
Import-Module ShoreTelDirector -Force -Verbose

$session = Send-ShoreTelDirectorLogin -shoretelServer "x.x.x.x" -windowsAuth $true
#$session = Send-ShoreTelDirectorLogin -shoretelServer "x.x.x.x" -windowsAuth $false -user "myuser" -pass "password"
$session.Authenticated

$extension = Read-Host "Extension to delete"
$confirm = Read-Host "Are you really sure you want to delete this user (this action can not be undone if it succeeds) (Y/N)"

if($confirm -eq 'Y')
{
    Remove-ShoreTelDirectorUser -extension $extension -session $session
}