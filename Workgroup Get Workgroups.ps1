param
(
    $groupExt = (Read-Host "Enter workgroup extension"),
    $adauth = $true,
    $user = "shoreteluser",
    $pass = "shoretelpass",
    $server = ""
)

$Error.Clear()

# Get the current script path
function Get-ScriptPath 
{
    if ($PSScriptRoot) { return $PSScriptRoot } 
    elseif ($psISE) { return split-path -parent $psISE.CurrentFile.Fullpath }
    else { return Split-Path ((Get-Variable MyInvocation -Scope 1).Value).MyCommand.Path }
}
$path = Get-ScriptPath

# if required add it to the module load path to environment variable
if (!($env:PSModulePath -Split ";" -like $path)) { $env:PSModulePath += ";$path" }

# Import the shoretel module
Import-Module ShoreTel -Force -Verbose

if($server -eq "")
{
    $server = Read-Host "Enter shoretel server computer name or ip address"
}

if($adauth)
{
    $session = New-ShoreTel_Session -shoretelServer $server -windowsAuth $true
}
else
{
    $session = New-ShoreTel_Session -shoretelServer $server -windowsAuth $false -user $user -pass $pass
}

# Check session created successfully
if(!$session.LoggedIn -or !$session.Authenticated)
{
    Write-Error "Unable to login or authenticate"
    read-host "Press any key to continue"
    return
}

# Get workgroup list
$workgroups = Get-ShoreTel_Workgroups -session $session

# Find specific workgroup
$workgroup = $workgroups | Where-Object {$_."group-ext" -eq $groupExt}

# Validation
if(!$workgroup)
{
    Write-Error "User is not part of the requested work group ext"
    read-host "Press any key to continue"
    return
}
if($workgroup.GetType().Name -ne "Hashtable")
{
    Write-Error "User is not part of the requested work group ext or multiple workgroups matched"
    read-host "Press any key to continue"
    return
}

# Change work group CHM
$result = Set-ShoreTel_WorkgroupCallHandlingMode -CHM $chm -queueId $workgroup."group-id" -session $session
if($result -eq $true)
{
    Write-Output "Success"
}
else
{
    Write-Output "Failure"
}

read-host "Press any key to continue"