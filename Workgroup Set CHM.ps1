param
(
    $adauth = $false,
    $user = "",
    $pass = "",
    $server = "",
    $groupExt = "",
    $chm = $null
)

$Error.Clear()

# Get the current script path
function Get-ScriptPath 
{
    if ($PSScriptRoot) { return $PSScriptRoot } 
    elseif ($psISE) { return split-path -parent $psISE.CurrentFile.Fullpath }
    else { return Split-Path ((Get-Variable MyInvocation -Scope 1).Value).MyCommand.Path }
}
$path = Get-ScriptPath

# if required add it to the module load path to environment variable
if (!($env:PSModulePath -Split ";" -like $path)) { $env:PSModulePath += ";$path" }

# Import the shoretel module
Import-Module ShoreTel -Force -Verbose

if($server -eq "")
{
    $server = Read-Host "Enter shoretel server computer name or ip address"
}

if($adauth -eq $false -and $user -eq "")
{
    $user = Read-Host "Enter shoretel user"
}

if($adauth -eq $false -and $pass -eq "")
{
	$pass = Read-Host "Enter shoretel pass" 
}

if($groupExt -eq "")
{
    $groupExt = Read-Host "Enter work group extension"
}

if($CHM -eq $null)
{
    $CHM = Read-Host "Enter the new CHM"
}

# Create session
$session = New-ShoreTel_Session -shoretelServer $server -windowsAuth $adauth -user $user -pass $pass

# Check session created successfully
if(!$session.LoggedIn -or !$session.Authenticated)
{
    Write-Error "Unable to login or authenticate"
    return
}

# Get workgroup list
$workgroups = Get-ShoreTel_Workgroups -session $session

# Find specific workgroup
$workgroup = $workgroups | Where-Object {$_."group-ext" -eq $groupExt}

# Validation
if(!$workgroup)
{
    Write-Error "User is not part of the requested work group ext"
    return
}
if($workgroup.GetType().Name -ne "Hashtable")
{
    Write-Error "User is not part of the requested work group ext or multiple workgroups matched"
    return
}

# Change work group CHM
$result = Set-ShoreTel_WorkgroupCallHandlingMode -CHM $chm -queueId $workgroup."group-id" -session $session
if($result -eq $true)
{
    Write-Output "Success"
}
else
{
    Write-Output "Failure"
}