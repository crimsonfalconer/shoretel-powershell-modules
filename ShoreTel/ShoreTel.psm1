﻿#$DebugPreference = "Continue"

function New-WC
{
  <#
  .SYNOPSIS
  Returns a new webclient object
  .DESCRIPTION
  Returns a new webclient object, 
  with the header "Content-Type", "text/xml; charset=utf-8" added
  set to use the default windows credentials
  .EXAMPLE
  $webclient = New-WC
  #>
    param 
    (
        [bool] $windowsAuth,
        [bool] $Expect100Continue
    )
    $webClient = New-Object net.webClient
    $webClient.Headers.add("Content-Type", "text/xml; charset=utf-8")

    if($Expect100Continue)
    {
        #$webClient.
    }

    if($windowsAuth) 
    {
        $webClient.UseDefaultCredentials = $true
    }
    else
    {
        $webClient.UseDefaultCredentials = $false
    }
    return $webClient
}


function Get-JSON 
{
  <#
  .SYNOPSIS
  Gets a web url then converts the results from JSON to a powershell object
  .DESCRIPTION
  Uses a web client object to get a web url expecting json string as the results.
  Converts the results from JSON to a powershell object then returns the object.
  .EXAMPLE
  Get-JSON -URL "http://myserver/myjsonfunction -webClient $webClient
  .EXAMPLE
  Get-JSON -URL "http://myserver/myjsonfunction2 -webClient (New-WC)
  .PARAMETER URL
  URL to get
  .PARAMETER webClient
  webClient .NET object
  #>
    param 
    (
        [string] $URL,
        [object] $webClient
    )

    $results =  (Get-Web -URL $URL -webClient $webClient)
    
    if ($results) { return ConvertFrom-Json $results }
    else { return $false }
}

function Send-JSON_Post
{
    param 
    (
        [string] $URL, 
        [object] $postData, 
        [object] $webClient
    )

    $postData = ConvertTo-Json $postData -Compress
    Write-Verbose ("Posting to: " + $URL + ":  Data:" + $postData)
    $results =  Send-Web_Post -URL $URL -postData $postData -webClient $webClient
    Write-Verbose ("Returned results: " + $results)
    if($results) { return ConvertFrom-Json $results }
    else { return $false }
}

function Send-Web_Post
{
    param (
        [string] $URL, 
        [string] $postData, 
        [object] $webClient
    )
    Write-Debug $URL
    Write-Debug $postData

    $res = $webClient.UploadString($URL, $postData)
    if($res) { return $res }
    else { return $false }
}

function Get-Web
{
  <#
  .SYNOPSIS
  Download a results from a URL as a string
  .DESCRIPTION
  Uses the DownloadString method of a weblclient to retrieve URL results and return them as a string.
  .EXAMPLE
  Get-Web -URL "http://google.com" -webClient $webClient
  .EXAMPLE
  Get-Web -URL "http://myserver/" -webClient (New-WC)
  .PARAMETER URL
  URL to download/get
  .PARAMETER webClient
  webClient .NET object
  #>
    param 
    (
        [string] $URL,
        [object] $webClient
    )

    Write-Debug $URL

    return $webClient.DownloadString($URL)
}

function Convert-ShoreTel_Array
{
    param 
    (
        [Parameter(ValueFromPipeline=$true)] [object] $array
    )

    $base = $array[0]
    $newArray = @()  
    for($i = 1; $i -lt $array.Count; $i++)
    {
        Write-Verbose("" + $i + ":" + $array[$i])
        $newArray += New-AssociateArray -arrKeys $base -arrValues $array[$i]
    }
    return ,$newArray
}

function New-AssociateArray
{
    param
    (
        [object] $arrKeys,
        [object] $arrValues
    )
    $arr = @{}
    for($i = 0; $i -lt $arrValues.Count; $i++)
    {
        
        if($arrValues[$i].GetType().Name -eq "Object[]")
        {
            $arr.($arrKeys[$i]) = Convert-ShoreTel_Array -array $arrValues[$i]
        }
        else
        {
            Write-Verbose($arrKeys[$i] + "=" + $arrValues[$i])
            $arr.($arrKeys[$i]) = $arrValues[$i]
        }

    }
    return ,$arr
}

function New-SequenceId 
{
  <#
  .SYNOPSIS
  Generate a new sequence id
  .DESCRIPTION
  Uses the DownloadString method of a weblclient to retrieve URL results and return them as a string.
  .EXAMPLE
  Get-Web -URL "http://google.com" -webClient $webClient
  .EXAMPLE
  Get-Web -URL "http://myserver/" -webClient (New-WC)
  .PARAMETER URL
  URL to download/get
  .PARAMETER webClient
  webClient .NET object
  #>
    param 
    (
        $session
    )
    $session.SequenceId += 1
    return $session.SequenceId
}

function New-ShoreTel_Session 
{
  <#
  .SYNOPSIS
  Create a new shoretel session object
  .DESCRIPTION
  Creates and returns a custom shoretel session object containing the
   - Session Id
   - Server
   - Error code/description
   - Web client .net object
   - Authenticated flag
   - Logged In flag
   - Sequence Id variable
   - User Id
   - User role
   - User dn (extension number)
  .EXAMPLE
  Get-Web -URL "http://google.com" -webClient $webClient
  .EXAMPLE
  Get-Web -URL "http://myserver/" -webClient (New-WC)
  .PARAMETER shoreTelServer
  IP address or hostname of the shoretel server
  #>
    param 
    (
        [string] $shoretelServer,
        [bool] $windowsAuth,
        [string] $user,
        [string] $pass
    )

    if($shoretelServer -eq ""){Throw "You must provide a ShoreTel server ip or hostname" }
    if($windowsAuth -eq $false -and ($user -eq "" -or $pass -eq "")){ Throw "You must provide a username or password if not using windows authentication" }

    $session = @{}
    $session.Server = $shoretelServer
    
    $session.Authenticated = $false
    $session.LoggedIn = $false
    $session.SequenceId = 1

    $queryString = "?app-id=ShoreTelPSModules"

    if($windowsAuth)
    {
        $session.webClient = New-WC -windowsAuth $true
        $response = Get-JSON -url ("http://" + $shoretelServer + "/sm_login_iwa/sm_login.asp" + $queryString) -webClient $session.webClient
    }
    else
    {
        
        $session.webClient = New-WC -windowsAuth $false
        $queryString += "&user-id=" + $user
        $md5Pass = ConvertTo-MD5 -string $pass
        $queryString += "&password=" + (Convert-ToJumbleHex -text $md5Pass)
        $queryString += "&ismd5=1"
        $response = Get-JSON -url ("http://" + $shoretelServer + "/sm_login/sm_login.asp" + $queryString) $postData -webClient $session.webClient
    }

    $session.Error = $response.error
    $session.ErrorDescription = ""

    if($response.error -eq 0) 
    { 
        $session.Authenticated = $true
        $session.SessionId = $response.results."session-id"
        $session.UserId = $response.results."user-id"
        $session.UserRole = $response.results."user-role"
        $session.UserDN = $response.results."user-dn"
        $null = Send-ShoreTel_CommunicatorLogin -session $session
    }
    else
    {
        Switch($response.error)
        {
            1 { $session.ErrorDescription = "Invalid ShoreTel User" }
            2 { $session.ErrorDescription = "Authentication failed" }
            3 { $session.ErrorDescription = "Failed to create session" }
            4 { $session.ErrorDescription = "User not AD enabled" }
            5 { $session.ErrorDescription = "Invalid request" }
            6 { $session.ErrorDescription = "Unauthorized IP address" }
            7 { $session.ErrorDescription = "System not AD enabled" }
            8 { $session.ErrorDescription = "Invalid security token" }
            9 { $session.ErrorDescription = "Invalid session" }
            10 { $session.ErrorDescription = "Invalid chars in login" }
            11 { $session.ErrorDescription = "not supported on system" }
            12 { $session.ErrorDescription = "Rejected due to delay" }
        }
        $session.Authenticated = $false
        Write-Error $session.ErrorDescription
    }
    return $session
}

Function ConvertTo-MD5($string) {
    $md5 = new-object -TypeName System.Security.Cryptography.MD5CryptoServiceProvider
    $utf8 = new-object -TypeName System.Text.UTF8Encoding
    $hash = [System.BitConverter]::ToString($md5.ComputeHash($utf8.GetBytes($String))).Replace("-", "")
    return $hash.ToLower()
}

function Send-ShoreTel_CommunicatorLogin
{
  <#
  .SYNOPSIS
  Authenticate the shoretel session.
  .DESCRIPTION
  Authenticate against the shoretel server using the custom session object created with the New-Shoretel_Session function.
  .EXAMPLE
  Send-ShoreTel_Login -windowsAuth $true -session $session
  .PARAMETER windowsAuth
  Authenticate using pass-through credentials
  .PARAMETER session
  Shoretel custom Session object. Created with the New-Shoretel_Session function.
  #>
    param 
    (
        [object] $session
    )
    
    if($session.LoggedIn -eq $false)
    {
        $loginPost = @{ username=$session.UserId; "user-auth-token"=$session.SessionId; "user-role"=$session.UserRole }
        $url = "http://" + $session.Server + ":5447/Login"
        $results = Send-JSON_Post -URL $url -postData $loginPost -webClient $session.webClient

        if ($results) 
        {
            if($results.error -eq 0)
            {
                $session.AuthId = $results.SessionId
                $session.LoggedIn = $true
                return $true
            }
            else
            {
                Write-Debug $results
                $session.ErrorDescription = "Login failed"
                Write-Error $session.ErrorDescription
                return $false
            }
        }
        else
        {
            $session.ErrorDescription = "No response recieved during login"
            Write-Error $session.ErrorDescription
            return $false
        }
    }
    else
    {
        $session.ErrorDescription = "Already logged in"
        Write-Error $session.ErrorDescription
        return $false
    }
}

Function New-ShoreTel_ExecuteMessage 
{
  <#
  .SYNOPSIS
  Create ans object representing a JSON execute message.
  .DESCRIPTION
  Create ans object representing a JSON execute message, containing a sequenceId, topic and message
  .EXAMPLE
  Send-ShoreTel_Login -windowsAuth $true -session $session
  .PARAMETER sequenceId
  Number incremented by New-SequenceId function
  .PARAMETER topic
  message topic
  .PARAMETER message
  message or command (examples: make-call)
  #>
    param 
    (
        [int] $sequenceId, 
        [string] $topic, 
        [string] $message
    )
    return @{"sequence-id"=$sequenceId; topic=$topic; message=$message}
}

function Send-ShoreTel_ExecuteMessage 
{
  <#
  .SYNOPSIS
  Send a shoretel execute message
  .DESCRIPTION
  Send a shoretel execute message, this uses both the custom session object and the custom execute message object 
  (the message object is convereted to JSON before being sent).
  .EXAMPLE
  Send-ShoreTel_ExecuteMessage -postData $data -session $session -returnResults $true
  .EXAMPLE
  Send-ShoreTel_ExecuteMessage -postData $data -session $session -returnResults $false
  .PARAMETER postData
  The object to convert to a json object
  .PARAMETER returnResults
  Set to true if you want the results of the execute message to be returned. Otherwise it returns true if the message completes without errors.
  .PARAMETER session
  Shoretel custom Session object. Created with the New-Shoretel_Session function.
  #>
    param 
    (
        [object] $postData,
        [switch] $returnResults = $false,
        [object] $session
    )
    $response = Send-JSON_Post -URL ("http://" + $session.Server + ":5447/Execute?SessionId=" + $session.AuthId) -postData $postData -webClient $session.webClient -verbose $PSBoundParameters['Verbose']
    
    if($returnResults) 
    {
        return $response
    }
    else
    {
        Write-Debug $response
        if($response.response -gt -1) {return $true} else {return $false}
    }
}

function Get-Events
{
    param
    (
        [Parameter(ValueFromPipeline=$true)] [object] $session
    )
    $response = Get-JSON -URL ("http://" + $session.Server + ":5447/GetEvents?SessionId=" + $session.AuthId + "&timeout=60&merge-pending=yes") -webClient $session.webClient
    return $response
}

function New-ShoreTel_CallMailbox
{
    param 
    (
        [Parameter(Mandatory=$true)]
        [string] $extension,
        
        [Parameter(ValueFromPipeline=$true)]
        [object] $session,

        [string] $shoretelServer,
        [bool] $windowsAuth,
        [string] $user,
        [string] $pass
    )
        if($session -eq $null){ Write-Error "Session object must be provided"; return}
        $postData = New-ShoreTel_ExecuteMessage -sequenceId (New-SequenceId $session) -topic "tel" -message "make-call-mb"
        $postData.dest = $extension
        $postData."guided-conf-transfer" = $true
        return Send-ShoreTel_ExecuteMessage  -postData $postData -session $session -verbose $PSBoundParameters['Verbose']
}

function New-ShoreTel_Call 
{
  <#
  .SYNOPSIS
  Make a voice call.
  .DESCRIPTION
  Make a voice call to the provided phone number.
  .EXAMPLE
  New-ShoreTel_Call -phoneNumber 0211111111 -session $session
  .EXAMPLE
  $session | New-ShoreTel_call -phoneNumber 5555
  .PARAMETER phoneNumber
  The phone number to call.
  .PARAMETER shoretelServer
  The ip address or hostname of the windows server. This parameter must be provided if an existing session has not be established.
  .PARAMETER windowsAuth
  Controls whether or not to authenticate using your windows credentials. This parameter must be provided if an existing session has not be established.
  .PARAMETER user
  User name to authenticate with. This parameter should only be provided and is mandatory if windows authentication is disabled.
  .PARAMETER pass
  Password to authenticate with. This parameter should only be provided and is mandatory if windows authentication is disabled.
  .PARAMETER session
  Shoretel custom Session object. Created with the New-Shoretel_Session function. This may also be piped to the function.
  #>
    param 
    (
        [Parameter(Mandatory=$true)]
        [string] $phoneNumber,

        [Parameter(ValueFromPipeline=$true)]
        [object] $session,

        [string] $shoretelServer,
        [bool] $windowsAuth,
        [string] $user,
        [string] $pass
    )
        if($shoretelServer -ne ""){$session = New-ShoreTel_Session -shoretelServer $shoretelServer -windowsAuth $windowsAuth -user $user -pass $pass}
        elseif($session -eq $null){Write-Error "Session object must be provided"; return}
        if($session.LoggedIn -eq $false -or $session.Authenticated -eq $false){Write-Error "Session must be logged in and authenticated"; return}

        $postData = New-ShoreTel_ExecuteMessage -sequenceId (New-SequenceId $session) -topic "tel" -message "make-call"
        $postData.dest = $phoneNumber
        return Send-ShoreTel_ExecuteMessage  -postData $postData -session $session -verbose $PSBoundParameters['Verbose']
}

function Stop-ShoreTel_Call 
{
  <#
  .SYNOPSIS
  End a shoretel call.
  .DESCRIPTION
  End the shoretel call matching the callId parameter.
  .EXAMPLE
  $calls = Get-ShoreTel_Calls -session $session
  Stop-ShoreTel_Call -callId $calls[0]."call-id" -session $session
  .EXAMPLE
  Stop-ShoreTel_Call -callId "xxxxxxx" -session $session
  .PARAMETER callId
  The id number of the call to terminate.
  .PARAMETER shoretelServer
  The ip address or hostname of the windows server. This parameter must be provided if an existing session has not be established.
  .PARAMETER windowsAuth
  Controls whether or not to authenticate using your windows credentials. This parameter must be provided if an existing session has not be established.
  .PARAMETER user
  User name to authenticate with. This parameter should only be provided and is mandatory if windows authentication is disabled.
  .PARAMETER pass
  Password to authenticate with. This parameter should only be provided and is mandatory if windows authentication is disabled.
  .PARAMETER session
  Shoretel custom Session object. Created with the New-Shoretel_Session function. This may also be piped to the function.
  #>
    param 
    (
        [Parameter(Mandatory=$true)]
        [string] $callId,

        [Parameter(ValueFromPipeline=$true)] 
        [object] $session,
        
        [string] $shoretelServer,
        [bool] $windowsAuth,
        [string] $user,
        [string] $pass
    )
        if($shoretelServer -ne ""){$session = New-ShoreTel_Session -shoretelServer $shoretelServer -windowsAuth $windowsAuth -user $user -pass $pass}
        elseif($session -eq $null){Write-Error "Session object must be provided"; return}
        if($session.LoggedIn -eq $false -or $session.Authenticated -eq $false){Write-Error "Session must be logged in and authenticated"; return}

        $postData = New-ShoreTel_ExecuteMessage -sequenceId (New-SequenceId $session) -topic "tel" -message "drop"
        $postData."call-id" = $callId
        return Send-ShoreTel_ExecuteMessage -postData $postData -session $session -verbose $PSBoundParameters['Verbose']
}

function Get-ShoreTel_CallHandlingMode 
{
  <#
  .SYNOPSIS
  Get the current users call handling mode.
  .DESCRIPTION
  Get the current users call handling mode. 1 = Standard, 2 = In a Meeting, 3 = Out Of Office, 4 = Extended Absence, 5 = Custom
  .EXAMPLE
  Get-ShoreTel_CallHandlingMode -session $session
  .EXAMPLE
  $session | Get-ShoreTel_CallHandlingMode
  .PARAMETER shoretelServer
  The ip address or hostname of the windows server. This parameter must be provided if an existing session has not be established.
  .PARAMETER windowsAuth
  Controls whether or not to authenticate using your windows credentials. This parameter must be provided if an existing session has not be established.
  .PARAMETER user
  User name to authenticate with. This parameter should only be provided and is mandatory if windows authentication is disabled.
  .PARAMETER pass
  Password to authenticate with. This parameter should only be provided and is mandatory if windows authentication is disabled.
  .PARAMETER session
  Shoretel custom Session object. Created with the New-Shoretel_Session function. This may also be piped to the function.
  #>
    param 
    (
        [Parameter(ValueFromPipeline=$true)] 
        [object] $session,

        [string] $shoretelServer,
        [bool] $windowsAuth,
        [string] $user,
        [string] $pass
    )
    if($shoretelServer -ne ""){$session = New-ShoreTel_Session -shoretelServer $shoretelServer -windowsAuth $windowsAuth -user $user -pass $pass}
    elseif($session -eq $null){Write-Error "Session object must be provided"; return}
    if($session.LoggedIn -eq $false -or $session.Authenticated -eq $false){Write-Error "Session must be logged in and authenticated"; return}

    $postData = New-ShoreTel_ExecuteMessage -sequenceId (New-SequenceId $session) -topic "config" -message "get-active-chm"
    if($ext)
    {
        $postData."user-ext" = $ext
    }
    return Send-ShoreTel_ExecuteMessage -postData $postData -session $session -verbose $PSBoundParameters['Verbose'] -returnResults
}

function Set-ShoreTel_CallHandlingMode 
{
  <#
  .SYNOPSIS
  Set the current users call handling mode.
  .DESCRIPTION
  Set the current users call handling mode. 1 = Standard, 2 = In a Meeting, 3 = Out Of Office, 4 = Extended Absence, 5 = Custom
  .EXAMPLE
  Set-ShoreTel_CallHandlingMode -session $session -CHM 1
  .EXAMPLE
  $session | Set-ShoreTel_CallHandlingMode -CHM 3
  .EXAMPLE
  $session | Set-ShoreTel_CallHandlingMode -CHM 3 -ext 1111
  .PARAMETER CHM
  The required call handling mode. 1 = Standard, 2 = In a Meeting, 3 = Out Of Office, 4 = Extended Absence, 5 = Custom
  .PARAMETER shoretelServer
  The ip address or hostname of the windows server. This parameter must be provided if an existing session has not be established.
  .PARAMETER windowsAuth
  Controls whether or not to authenticate using your windows credentials. This parameter must be provided if an existing session has not be established.
  .PARAMETER user
  User name to authenticate with. This parameter should only be provided and is mandatory if windows authentication is disabled.
  .PARAMETER pass
  Password to authenticate with. This parameter should only be provided and is mandatory if windows authentication is disabled.
  .PARAMETER session
  Shoretel custom Session object. Created with the New-Shoretel_Session function. This may also be piped to the function.
  #>
    param 
    (
        [Parameter(Mandatory=$true)]
        [int] $CHM,
        [string] $ext,

        [Parameter(ValueFromPipeline=$true)] 
        [object] $session,

        [string] $shoretelServer,
        [bool] $windowsAuth,
        [string] $user,
        [string] $pass
    )
    if($shoretelServer -ne ""){$session = New-ShoreTel_Session -shoretelServer $shoretelServer -windowsAuth $windowsAuth -user $user -pass $pass}
    elseif($session -eq $null){Write-Error "Session object must be provided"; return}
    if($session.LoggedIn -eq $false -or $session.Authenticated -eq $false){Write-Error "Session must be logged in and authenticated"; return}

    $postData = New-ShoreTel_ExecuteMessage -sequenceId (New-SequenceId $session) -topic "config" -message "set-active-chm"
    $postData.mode = $CHM
    if($ext)
    {
        $postData."user-ext" = $ext
    }
    return Send-ShoreTel_ExecuteMessage -postData $postData -session $session -verbose $PSBoundParameters['Verbose']
}

function Get-ShoreTel_ExtensionProperties
{
  <#
  .SYNOPSIS
  Get the extension properties for the current user.
  .DESCRIPTION
  Get the extension properties for the current user.
  .EXAMPLE
  Get-ShoreTel_ExtensionProperties  -session $session
  .PARAMETER shoretelServer
  The ip address or hostname of the windows server. This parameter must be provided if an existing session has not be established.
  .PARAMETER windowsAuth
  Controls whether or not to authenticate using your windows credentials. This parameter must be provided if an existing session has not be established.
  .PARAMETER user
  User name to authenticate with. This parameter should only be provided and is mandatory if windows authentication is disabled.
  .PARAMETER pass
  Password to authenticate with. This parameter should only be provided and is mandatory if windows authentication is disabled.
  .EXAMPLE
  $session | Get-ShoreTel_ExtensionProperties
  .PARAMETER session
  Shoretel custom Session object. Created with the New-Shoretel_Session function. This may also be piped to the function.
  #>
    param 
    (
        [Parameter(ValueFromPipeline=$true)] 
        [object] $session,

        [string] $shoretelServer,
        [bool] $windowsAuth,
        [string] $user,
        [string] $pass
    )
    if($shoretelServer -ne ""){$session = New-ShoreTel_Session -shoretelServer $shoretelServer -windowsAuth $windowsAuth -user $user -pass $pass}
    elseif($session -eq $null){Write-Error "Session object must be provided"; return}
    if($session.LoggedIn -eq $false -or $session.Authenticated -eq $false){Write-Error "Session must be logged in and authenticated"; return}

    $postData = New-ShoreTel_ExecuteMessage -seq (New-SequenceId $session) -topic "tel" -message "get-ext-props"
    $response = Send-ShoreTel_ExecuteMessage  -postData $postData -session $session -returnResults
    return $response
}

function Set-ShoreTel_PhoneAssignment 
{
  <#
  .SYNOPSIS
  Set external phone assignment for the current user.
  .DESCRIPTION
  Set external phone assignment for the current user. Use the sendHome switch to reassign the extension to its home phone.
  .EXAMPLE
  Set-ShoreTel_PhoneAssignment  -session $session -externalNumber 'xxxxxxxxxx'
  .EXAMPLE
  Set-ShoreTel_PhoneAssignment  -session $session -sendHome
  .EXAMPLE
  $session | Set-ShoreTel_PhoneAssignment -externalNumber 'xxxxxxxxxx'
  .PARAMETER externalNumber
  The external phone number to assign to the extension.
  .PARAMETER sendHome
  Switch parameter. Used to send an extension back to its home phone.
  .PARAMETER session
  Shoretel custom Session object. Created with the New-Shoretel_Session function. This may also be piped to the function.
  #>
    param 
    (
        [string] $externalNumber,
        [switch] $sendHome,

        [Parameter(ValueFromPipeline=$true)] 
        [object] $session,

        [string] $shoretelServer,
        [bool] $windowsAuth,
        [string] $user,
        [string] $pass
    )
    if($shoretelServer -ne ""){$session = New-ShoreTel_Session -shoretelServer $shoretelServer -windowsAuth $windowsAuth -user $user -pass $pass}
    elseif($session -eq $null){Write-Error "Session object must be provided"; return}
    if($session.LoggedIn -eq $false -or $session.Authenticated -eq $false){Write-Error "Session must be logged in and authenticated"; return}

    if($sendHome)
    {
        $postData = New-ShoreTel_ExecuteMessage -sequenceId (New-SequenceId $session) -topic "tel" -message "assign-home"
    }
    else
    {
        $postData = New-ShoreTel_ExecuteMessage -sequenceId (New-SequenceId $session) -topic "tel" -message "assign-external"
        $postData."external-number" = $num
    }

    return Send-ShoreTel_ExecuteMessage -postData $postData -session $session
}

function Search-ShoreTel_Contacts 
{
    param
    (
        [string] $searchFor,
        [Parameter(ValueFromPipeline=$true)] 
        [object] $session,

        [string] $shoretelServer,
        [bool] $windowsAuth,
        [string] $user,
        [string] $pass
    )
    if($shoretelServer -ne ""){$session = New-ShoreTel_Session -shoretelServer $shoretelServer -windowsAuth $windowsAuth -user $user -pass $pass}
    elseif($session -eq $null){Write-Error "Session object must be provided"; return}
    if($session.LoggedIn -eq $false -or $session.Authenticated -eq $false){Write-Error "Session must be logged in and authenticated"; return}

    $postData = New-ShoreTel_ExecuteMessage -seq (New-SequenceId $session) -topic "find" -message "lookup-contacts"
    $postData."tokens" = $searchFor

    $response = Send-ShoreTel_ExecuteMessage  -postData $postData -session $session -returnResults
    Write-Verbose $response

    $contacts = Convert-ShoreTel_Array -array $response.contacts
    return ,$contacts
}

function Get-ShoreTel_Calls
{
    param 
    (
        [Parameter(ValueFromPipeline=$true)] 
        [object] $session,

        [string] $shoretelServer,
        [bool] $windowsAuth,
        [string] $user,
        [string] $pass
    )
    if($shoretelServer -ne ""){$session = New-ShoreTel_Session -shoretelServer $shoretelServer -windowsAuth $windowsAuth -user $user -pass $pass}
    elseif($session -eq $null){Write-Error "Session object must be provided"; return}
    if($session.LoggedIn -eq $false -or $session.Authenticated -eq $false){Write-Error "Session must be logged in and authenticated"; return}

    $postData = New-ShoreTel_ExecuteMessage -seq (New-SequenceId $session) -topic "tel" -message "get-calls"
    $response = Send-ShoreTel_ExecuteMessage  -postData $postData -session $session -returnResults
    if($response.calls)
    {
        return ,($response.calls)
    }
    return $false
}

function Get-ShoreTel_VMAccessibleMailboxes
{
    param
    (
        [Parameter(ValueFromPipeline=$true)] 
        [object] $session,

        [string] $shoretelServer,
        [bool] $windowsAuth,
        [string] $user,
        [string] $pass
    )
    if($shoretelServer -ne ""){$session = New-ShoreTel_Session -shoretelServer $shoretelServer -windowsAuth $windowsAuth -user $user -pass $pass}
    elseif($session -eq $null){Write-Error "Session object must be provided"; return}
    if($session.LoggedIn -eq $false -or $session.Authenticated -eq $false){Write-Error "Session must be logged in and authenticated"; return}

    $postData = New-ShoreTel_ExecuteMessage -seq (New-SequenceId $session) -topic "vm" -message "get-accessible-mailboxes"
    $response = Send-ShoreTel_ExecuteMessage  -postData $postData -session $session -returnResults
    if($response."accessible-mailboxes")
    {
        return ,(Convert-ShoreTel_Array -array $response."accessible-mailboxes")
    }
    return $false
}

function Get-ShoreTel_VMList
{
    param
    (
        [string] $mailboxId,

        [Parameter(ValueFromPipeline=$true)] 
        [object] $session,

        [string] $shoretelServer,
        [bool] $windowsAuth,
        [string] $user,
        [string] $pass
    )
    if($shoretelServer -ne ""){$session = New-ShoreTel_Session -shoretelServer $shoretelServer -windowsAuth $windowsAuth -user $user -pass $pass}
    elseif($session -eq $null){Write-Error "Session object must be provided"; return}
    if($session.LoggedIn -eq $false -or $session.Authenticated -eq $false){Write-Error "Session must be logged in and authenticated"; return}

    $postData = New-ShoreTel_ExecuteMessage -seq (New-SequenceId $session) -topic "vm" -message "get-msgs"
    if($mailboxId) {$postData."mbox-id" = $mailboxId}
    $response = Send-ShoreTel_ExecuteMessage  -postData $postData -session $session -returnResults
    if($response."vm-messages")
    {
        
        return ,(Convert-ShoreTel_Array -array $response."vm-messages")
    }
    return $false
}

function Send-ShoreTel_PlayVM
{
  <#
  .SYNOPSIS
  Play a Shoretel voice mail.
  .DESCRIPTION
  Play a ShoreTel voice mail using a the current users assigned phone.
  .EXAMPLE
  Play-ShoreTel_VM -session $session -msgIds $msgId -vmFolder 3
  .EXAMPLE
  $session | Play-ShoreTel_VM -msgId $msgId -vmFolder 2
  .PARAMETER msgId
  Voice mail message unique identifier.
  .PARAMETER mailboxId
  The extension/ mailbox number that contains the voice mail message
  .PARAMETER session
  Shoretel custom Session object. Created with the New-Shoretel_Session function. This may also be piped to the function.
  #>
    param 
    (
        [string] $msgId,
        [string] $mailboxId,

        [Parameter(ValueFromPipeline=$true)] 
        [object] $session,

        [string] $shoretelServer,
        [bool] $windowsAuth,
        [string] $user,
        [string] $pass
    )
    if($shoretelServer -ne ""){$session = New-ShoreTel_Session -shoretelServer $shoretelServer -windowsAuth $windowsAuth -user $user -pass $pass}
    elseif($session -eq $null){Write-Error "Session object must be provided"; return}
    if($session.LoggedIn -eq $false -or $session.Authenticated -eq $false){Write-Error "Session must be logged in and authenticated"; return}

    $postData = New-ShoreTel_ExecuteMessage -seq (New-SequenceId $session) -topic "vm" -message "play"
    $postData."msg-id" =  $msgId
    $postData."drop-call" = $false
    if($mailboxId) {$postData."mbox-id" = $mailboxId}
    $response = Send-ShoreTel_ExecuteMessage  -postData $postData -session $session
    return $response
}

function Set-ShoreTel_VMSubject
{
  <#
  .SYNOPSIS
  Set a Shoretel voice mails subject.
  .DESCRIPTION
  Set a Shoretel voice mails subject.
  .EXAMPLE
  Set-ShoreTel_VMSubject -session $session -msgId $msgId -subject "My Subject"
  .EXAMPLE
  $session | Set-ShoreTel_VMSubject -msgId $msgId -subject "My Subject"
  .PARAMETER msgId
  Unique ID of the message to move.
  .PARAMETER subject
  String to set the subject too.
  .PARAMETER mailboxId
  The extension/ mailbox number that contains the voice mail message
  .PARAMETER shoretelServer
  The ip address or hostname of the windows server. This parameter must be provided if an existing session has not be established.
  .PARAMETER windowsAuth
  Controls whether or not to authenticate using your windows credentials. This parameter must be provided if an existing session has not be established.
  .PARAMETER user
  User name to authenticate with. This parameter should only be provided and is mandatory if windows authentication is disabled.
  .PARAMETER pass
  Password to authenticate with. This parameter should only be provided and is mandatory if windows authentication is disabled.
  .PARAMETER session
  Shoretel custom Session object. Created with the New-Shoretel_Session function. This may also be piped to the function.
  #>
    param 
    (
        [string] $msgId,
        [string] $subject,

        [Parameter(ValueFromPipeline=$true)] 
        [object] $session,

        [string] $shoretelServer,
        [bool] $windowsAuth,
        [string] $user,
        [string] $pass
    )
    if($shoretelServer -ne ""){$session = New-ShoreTel_Session -shoretelServer $shoretelServer -windowsAuth $windowsAuth -user $user -pass $pass}
    elseif($session -eq $null){Write-Error "Session object must be provided"; return}
    if($session.LoggedIn -eq $false -or $session.Authenticated -eq $false){Write-Error "Session must be logged in and authenticated"; return}

    $postData = New-ShoreTel_ExecuteMessage -seq (New-SequenceId $session) -topic "vm" -message "set-subject"
    $postData."msg-id" += $msgId
    $postData.subject = $subject
    if($mailboxId) {$postData."mbox-id" = $mailboxId}
    $response = Send-ShoreTel_ExecuteMessage  -postData $postData -session $session
    return $response
}

function Move-ShoreTel_VM
{
  <#
  .SYNOPSIS
  Move a Shoretel voice mail.
  .DESCRIPTION
  Move a ShoreTel voice mail to a different folder.
  .EXAMPLE
  Move-ShoreTel_VM -session $session -msgIds $msgId -vmFolder 3
  .EXAMPLE
  $session | Move-ShoreTel_VM -msgIds $msgId -vmFolder 2
  .PARAMETER msgIds
  Messages Ids to move
  .PARAMETER vmFolder
  The folder to move the message to. 1 = Inbox, 2 = Saved, 3 = Deleted
  .PARAMETER mailboxId
  The extension/ mailbox number that contains the voice mail message
  .PARAMETER shoretelServer
  The ip address or hostname of the windows server. This parameter must be provided if an existing session has not be established.
  .PARAMETER windowsAuth
  Controls whether or not to authenticate using your windows credentials. This parameter must be provided if an existing session has not be established.
  .PARAMETER user
  User name to authenticate with. This parameter should only be provided and is mandatory if windows authentication is disabled.
  .PARAMETER pass
  Password to authenticate with. This parameter should only be provided and is mandatory if windows authentication is disabled.
  .PARAMETER session
  Shoretel custom Session object. Created with the New-Shoretel_Session function. This may also be piped to the function.
  #>
    param 
    (
        [string] $msgIds,
        [int] $vmFolder,

        [Parameter(ValueFromPipeline=$true)] 
        [object] $session,

        [string] $shoretelServer,
        [bool] $windowsAuth,
        [string] $user,
        [string] $pass
    )
    if($shoretelServer -ne ""){$session = New-ShoreTel_Session -shoretelServer $shoretelServer -windowsAuth $windowsAuth -user $user -pass $pass}
    elseif($session -eq $null){Write-Error "Session object must be provided"; return}
    if($session.LoggedIn -eq $false -or $session.Authenticated -eq $false){Write-Error "Session must be logged in and authenticated"; return}

    $postData = New-ShoreTel_ExecuteMessage -seq (New-SequenceId $session) -topic "vm" -message "move-msgs"
    $postData."msg-ids" = @()
    $postData."msg-ids" += $msgIds
    $postData."vm-folder" = $vmFolder
    if($mailboxId) {$postData."mbox-id" = $mailboxId}
    $response = Send-ShoreTel_ExecuteMessage  -postData $postData -session $session
    return $response
}

function Get-ShoreTel_VMFile
{
  <#
  .SYNOPSIS
  Download a ShoreTel voice mail message
  .DESCRIPTION
  Download a ShoreTel voice mail message as a wave file, requires a message id and session object
  .EXAMPLE
  Get-ShoreTel_VMFile -msgId $msgId -file "C:\Temp\Voice Mail.wav" -session $session
  .EXAMPLE
  Get-ShoreTel_VMFile -msgId $msgId $mailboxId "1234" -file "C:\Temp\Voice Mail.wav" -session $session
  .EXAMPLE
  $session | Get-ShoreTel_VMFile -msgId $msgId -file "C:\Temp\Voice Mail.wav" -session $session
  .PARAMETER msgId
  The voice mail message unique identifier
  .PARAMETER mailboxId
  The extension/ mailbox number that contains the voice mail message
  .PARAMETER file
  The filename to use when downloading the file
  .PARAMETER shoretelServer
  The ip address or hostname of the windows server. This parameter must be provided if an existing session has not be established.
  .PARAMETER windowsAuth
  Controls whether or not to authenticate using your windows credentials. This parameter must be provided if an existing session has not be established.
  .PARAMETER user
  User name to authenticate with. This parameter should only be provided and is mandatory if windows authentication is disabled.
  .PARAMETER pass
  Password to authenticate with. This parameter should only be provided and is mandatory if windows authentication is disabled.
  .PARAMETER session
  Shoretel custom Session object. Created with the New-Shoretel_Session function. This may also be piped to the function.
  #>
    param
    (
        [string] $msgId,
        [string] $mailboxId,
        [string] $file,

        [Parameter(ValueFromPipeline=$true)] 
        [object] $session,

        [string] $shoretelServer,
        [bool] $windowsAuth,
        [string] $user,
        [string] $pass
    )
    if($shoretelServer -ne ""){$session = New-ShoreTel_Session -shoretelServer $shoretelServer -windowsAuth $windowsAuth -user $user -pass $pass}
    elseif($session -eq $null){Write-Error "Session object must be provided"; return}
    if($session.LoggedIn -eq $false -or $session.Authenticated -eq $false){Write-Error "Session must be logged in and authenticated"; return}

    $postData = New-ShoreTel_ExecuteMessage -seq (New-SequenceId $session) -topic "vm" -message "prepare-download"
    $postData."request-id" = $postData."sequence-id"
    $postData."msg-id" = $msgId
    if($mailboxId){ $postData."mbox-id" = $mailboxId}
    else {$postData."mbox-id" = $session.UserDN}
    $response = Send-ShoreTel_ExecuteMessage -postData $postData -session $session -returnResults
    if($response.response -eq 0)
    {
        $events = Get-Events -session $session
        foreach($event in $events)
        {
            if($event."msg-id" -eq $msgId)
            {
                Write-Debug("Downloading file to: " + $file)
                $session.webClient.DownloadFile($event.location, $file)
                return $true
            }
        }
    }
    return $false
}

function Clear-ShoreTel_DeletedVM
{
  <#
  .SYNOPSIS
  Purges the delete voice mail folder
  .DESCRIPTION
  Purges the delete voice mail folder, for the current user.
  .EXAMPLE
  Clear-ShoreTel_DeletedVM -session $session
  .PARAMETER shoretelServer
  The ip address or hostname of the windows server. This parameter must be provided if an existing session has not be established.
  .PARAMETER windowsAuth
  Controls whether or not to authenticate using your windows credentials. This parameter must be provided if an existing session has not be established.
  .PARAMETER user
  User name to authenticate with. This parameter should only be provided and is mandatory if windows authentication is disabled.
  .PARAMETER pass
  Password to authenticate with. This parameter should only be provided and is mandatory if windows authentication is disabled.
  .EXAMPLE
  $session | Clear-ShoreTel_DeletedVM
  .PARAMETER session
  Shoretel custom Session object. Created with the New-Shoretel_Session function. This may also be piped to the function.
  #>
    param 
    (
        [Parameter(ValueFromPipeline=$true)] 
        [object] $session,

        [string] $shoretelServer,
        [bool] $windowsAuth,
        [string] $user,
        [string] $pass
    )
    if($shoretelServer -ne ""){$session = New-ShoreTel_Session -shoretelServer $shoretelServer -windowsAuth $windowsAuth -user $user -pass $pass}
    elseif($session -eq $null){Write-Error "Session object must be provided"; return}
    if($session.LoggedIn -eq $false -or $session.Authenticated -eq $false){Write-Error "Session must be logged in and authenticated"; return}

    $postData = New-ShoreTel_ExecuteMessage -seq (New-SequenceId $session) -topic "vm" -message "purge-deleted-msgs"
    $response = Send-ShoreTel_ExecuteMessage  -postData $postData -session $session
    return $response
}

function Start-ShoreTel_CallRecord
{
  <#
  .SYNOPSIS
  Starts a call recording.
  .DESCRIPTION
  Starts a call recording. Call to record is identified by CallId
  .EXAMPLE
  Start-ShoreTel_CallRecord -session $session -callId (CallId) -ext $session.userDN
  .EXAMPLE
  $session | Stop-ShoreTel_CallRecord -callId (CallId) -ext $session.userDN
  .PARAMETER CallId
  Call identifier.
  .PARAMETER Ext
  Must match the Users extension.
  .PARAMETER shoretelServer
  The ip address or hostname of the windows server. This parameter must be provided if an existing session has not be established.
  .PARAMETER windowsAuth
  Controls whether or not to authenticate using your windows credentials. This parameter must be provided if an existing session has not be established.
  .PARAMETER user
  User name to authenticate with. This parameter should only be provided and is mandatory if windows authentication is disabled.
  .PARAMETER pass
  Password to authenticate with. This parameter should only be provided and is mandatory if windows authentication is disabled.
  .PARAMETER session
  Shoretel custom Session object. Created with the New-Shoretel_Session function. This may also be piped to the function.
  #>
    param
    (
        [string] $callId,
        [string] $ext,
        [Parameter(ValueFromPipeline=$true)] 
        [object] $session,

        [string] $shoretelServer,
        [bool] $windowsAuth,
        [string] $user,
        [string] $pass
    )
    if($shoretelServer -ne ""){$session = New-ShoreTel_Session -shoretelServer $shoretelServer -windowsAuth $windowsAuth -user $user -pass $pass}
    elseif($session -eq $null){Write-Error "Session object must be provided"; return}
    if($session.LoggedIn -eq $false -or $session.Authenticated -eq $false){Write-Error "Session must be logged in and authenticated"; return}

    $postData = New-ShoreTel_ExecuteMessage -seq (New-SequenceId $session) -topic "tel" -message "record"
    $postData."call-id" = $callId
    $postData."ext" = $ext
    $response = Send-ShoreTel_ExecuteMessage  -postData $postData -session $session
    return $response
}

function Stop-ShoreTel_CallRecord
{
  <#
  .SYNOPSIS
  Stops the current call recording.
  .DESCRIPTION
  Stops the call recording. Call is identified by CallId
  .EXAMPLE
  Stop-ShoreTel_CallRecord -session $session -callId (CallId) -ext $session.userDN
  .EXAMPLE
  $session | Stop-ShoreTel_CallRecord -callId (CallId) -ext $session.userDN
  .PARAMETER CallId
  Call identifier.
  .PARAMETER Ext
  I think this is the extension to send the recording too, maybe.
  .PARAMETER shoretelServer
  The ip address or hostname of the windows server. This parameter must be provided if an existing session has not be established.
  .PARAMETER windowsAuth
  Controls whether or not to authenticate using your windows credentials. This parameter must be provided if an existing session has not be established.
  .PARAMETER user
  User name to authenticate with. This parameter should only be provided and is mandatory if windows authentication is disabled.
  .PARAMETER pass
  Password to authenticate with. This parameter should only be provided and is mandatory if windows authentication is disabled.
  .PARAMETER session
  Shoretel custom Session object. Created with the New-Shoretel_Session function. This may also be piped to the function.
  #>
    param
    (
        [string] $callId,
        [string] $ext,
        [Parameter(ValueFromPipeline=$true)] 
        [object] $session,

        [string] $shoretelServer,
        [bool] $windowsAuth,
        [string] $user,
        [string] $pass
    )
    if($shoretelServer -ne ""){$session = New-ShoreTel_Session -shoretelServer $shoretelServer -windowsAuth $windowsAuth -user $user -pass $pass}
    elseif($session -eq $null){Write-Error "Session object must be provided"; return}
    if($session.LoggedIn -eq $false -or $session.Authenticated -eq $false){Write-Error "Session must be logged in and authenticated"; return}

    $postData = New-ShoreTel_ExecuteMessage -seq (New-SequenceId $session) -topic "tel" -message "stop-record"
    $postData."call-id" = $callId
    $postData."ext" = $ext
    $response = Send-ShoreTel_ExecuteMessage  -postData $postData -session $session
    return $response
}

function Send-ShoreTel_CallDigit
{
  <#
  .SYNOPSIS
  Sends a digit over a shoretel call
  .DESCRIPTION
  Sends a digit over a ShoreTel call. Requires a call-id and digit to be sent.
  .EXAMPLE
  Send-ShoreTel_CallDigit -session $session -callId (CallId) -digit "1"
  .EXAMPLE
  $session | Send-ShoreTel_CallDigit -callId (CallId) -digits "1"
  .PARAMETER callId
  Call identifier.
  .PARAMETER digits
  Digits to send over the call.
  .PARAMETER shoretelServer
  The ip address or hostname of the windows server. This parameter must be provided if an existing session has not be established.
  .PARAMETER windowsAuth
  Controls whether or not to authenticate using your windows credentials. This parameter must be provided if an existing session has not be established.
  .PARAMETER user
  User name to authenticate with. This parameter should only be provided and is mandatory if windows authentication is disabled.
  .PARAMETER pass
  Password to authenticate with. This parameter should only be provided and is mandatory if windows authentication is disabled.
  .PARAMETER session
  Shoretel custom Session object. Created with the New-Shoretel_Session function. This may also be piped to the function.
  #>
    param
    (
        [string] $callId,
        [string] $digits,
        [Parameter(ValueFromPipeline=$true)] 
        [object] $session,

        [string] $shoretelServer,
        [bool] $windowsAuth,
        [string] $user,
        [string] $pass
    )
    if($shoretelServer -ne ""){$session = New-ShoreTel_Session -shoretelServer $shoretelServer -windowsAuth $windowsAuth -user $user -pass $pass}
    elseif($session -eq $null){Write-Error "Session object must be provided"; return}
    if($session.LoggedIn -eq $false -or $session.Authenticated -eq $false){Write-Error "Session must be logged in and authenticated"; return}

    $postData = New-ShoreTel_ExecuteMessage -seq (New-SequenceId $session) -topic "tel" -message "send-digits"
    $postData."call-id" = $callId
    $postData."digits" = $digits
    $response = Send-ShoreTel_ExecuteMessage  -postData $postData -session $session
    return $response
}

function Get-ShoreTel_Workgroups
{
  <#
  .SYNOPSIS
  Gets the available ShoreTel workgroups.
  .DESCRIPTION
  Gets the ShoreTel workgroups available to the current agent.
  .EXAMPLE
  Get-ShoreTel_Workgroups -session $session
  .EXAMPLE
  $session | Get-ShoreTel_Workgroups
  .PARAMETER shoretelServer
  The ip address or hostname of the windows server. This parameter must be provided if an existing session has not be established.
  .PARAMETER windowsAuth
  Controls whether or not to authenticate using your windows credentials. This parameter must be provided if an existing session has not be established.
  .PARAMETER user
  User name to authenticate with. This parameter should only be provided and is mandatory if windows authentication is disabled.
  .PARAMETER pass
  Password to authenticate with. This parameter should only be provided and is mandatory if windows authentication is disabled.
  .PARAMETER session
  Shoretel custom Session object. Created with the New-Shoretel_Session function. This may also be piped to the function.
  #>
    param
    (
        [Parameter(ValueFromPipeline=$true)] 
        [object] $session,

        [string] $shoretelServer,
        [bool] $windowsAuth,
        [string] $user,
        [string] $pass
    )
    if($shoretelServer -ne ""){$session = New-ShoreTel_Session -shoretelServer $shoretelServer -windowsAuth $windowsAuth -user $user -pass $pass}
    elseif($session -eq $null){Write-Error "Session object must be provided"; return}
    if($session.LoggedIn -eq $false -or $session.Authenticated -eq $false){Write-Error "Session must be logged in and authenticated"; return}

    $postData = New-ShoreTel_ExecuteMessage -seq (New-SequenceId $session) -topic "workgroup" -message "get-groups"
    $response = Send-ShoreTel_ExecuteMessage  -postData $postData -session $session -returnResults -verbose

    if($response.groups)
    {
        $groups = Convert-ShoreTel_Array -array $response.groups
        return ,$groups
    }
    return $false
}

function Get-ShoreTel_WorkgroupAgents
{
  <#
  .SYNOPSIS
  Gets the available ShoreTel workgroup agents.
  .DESCRIPTION
  Gets the available ShoreTel workgroup agents.
  .EXAMPLE
  Get-ShoreTel_WorkgroupAgents -session $session
  .EXAMPLE
  $session | Get-ShoreTel_WorkgroupAgents
  .PARAMETER shoretelServer
  The ip address or hostname of the windows server. This parameter must be provided if an existing session has not be established.
  .PARAMETER windowsAuth
  Controls whether or not to authenticate using your windows credentials. This parameter must be provided if an existing session has not be established.
  .PARAMETER user
  User name to authenticate with. This parameter should only be provided and is mandatory if windows authentication is disabled.
  .PARAMETER pass
  Password to authenticate with. This parameter should only be provided and is mandatory if windows authentication is disabled.
  .PARAMETER session
  Shoretel custom Session object. Created with the New-Shoretel_Session function. This may also be piped to the function.
  #>
    param
    (
        [Parameter(ValueFromPipeline=$true)] 
        [object] $session,

        [string] $shoretelServer,
        [bool] $windowsAuth,
        [string] $user,
        [string] $pass
    )
    if($shoretelServer -ne ""){$session = New-ShoreTel_Session -shoretelServer $shoretelServer -windowsAuth $windowsAuth -user $user -pass $pass}
    elseif($session -eq $null){Write-Error "Session object must be provided"; return}
    if($session.LoggedIn -eq $false -or $session.Authenticated -eq $false){Write-Error "Session must be logged in and authenticated"; return}

    $postData = New-ShoreTel_ExecuteMessage -seq (New-SequenceId $session) -topic "workgroup" -message "get-agents"
    $response = Send-ShoreTel_ExecuteMessage  -postData $postData -session $session -returnResults -verbose

    if($response.agents)
    {
        $agents = Convert-ShoreTel_Array -array $response.agents
        return ,$agents
    }
    return $false
}



function Set-ShoreTel_WorkgroupCallHandlingMode 
{
  <#
  .SYNOPSIS
  Set a workgroups call handling mode.
  .DESCRIPTION
  Set a workgroups call handling mode. 1 = On-Hours, 2 = Off-hours, 3 = Holiday, 4 = Custom
  .EXAMPLE
  Set-ShoreTel_WorkgroupCallHandlingMode -session $session -CHM 1
  .EXAMPLE
  $session | Set-ShoreTel_WorkgroupCallHandlingMode -CHM 3 -ext 1111
  .PARAMETER queueId
  The queue id to set the chm of.
  .PARAMETER CHM
  The required call handling mode. 1 = On-Hours, 2 = Off-hours, 3 = Holiday, 4 = Custom
  .PARAMETER shoretelServer
  The ip address or hostname of the windows server. This parameter must be provided if an existing session has not be established.
  .PARAMETER windowsAuth
  Controls whether or not to authenticate using your windows credentials. This parameter must be provided if an existing session has not be established.
  .PARAMETER user
  User name to authenticate with. This parameter should only be provided and is mandatory if windows authentication is disabled.
  .PARAMETER pass
  Password to authenticate with. This parameter should only be provided and is mandatory if windows authentication is disabled.
  .PARAMETER session
  Shoretel custom Session object. Created with the New-Shoretel_Session function. This may also be piped to the function.
  #>
    param 
    (
        [Parameter(Mandatory=$true)]
        [int] $CHM,

        [Parameter(Mandatory=$true)]
        [int] $queueId,

        [Parameter(ValueFromPipeline=$true)] 
        [object] $session,

        [string] $shoretelServer,
        [bool] $windowsAuth,
        [string] $user,
        [string] $pass
    )
    if($shoretelServer -ne ""){$session = New-ShoreTel_Session -shoretelServer $shoretelServer -windowsAuth $windowsAuth -user $user -pass $pass}
    elseif($session -eq $null){Write-Error "Session object must be provided"; return}
    if($session.LoggedIn -eq $false -or $session.Authenticated -eq $false){Write-Error "Session must be logged in and authenticated"; return}

    $postData = New-ShoreTel_ExecuteMessage -sequenceId (New-SequenceId $session) -topic "workgroup" -message "set-queue-chm"
    $postData.chm = $CHM
    $postData."queue-id" = $queueId
    $postData."request-id" = $postData."sequence-id"

    return Send-ShoreTel_ExecuteMessage -postData $postData -session $session -verbose $PSBoundParameters['Verbose']
}

function Set-ShoreTel_WorkgroupLogin
{
  <#
  .SYNOPSIS
  Login to ShoreTel workgroups
  .DESCRIPTION
  Login the current extension to ShoreTel workgroups
  .EXAMPLE
  Set-ShoreTel_WorkgroupLogin -session $session
  .EXAMPLE
  $session | Set-ShoreTel_WorkgroupLogin
  .PARAMETER shoretelServer
  The ip address or hostname of the windows server. This parameter must be provided if an existing session has not be established.
  .PARAMETER windowsAuth
  Controls whether or not to authenticate using your windows credentials. This parameter must be provided if an existing session has not be established.
  .PARAMETER user
  User name to authenticate with. This parameter should only be provided and is mandatory if windows authentication is disabled.
  .PARAMETER pass
  Password to authenticate with. This parameter should only be provided and is mandatory if windows authentication is disabled.
  .PARAMETER agentId
  Optional Agent id to login
  .PARAMETER session
  Shoretel custom Session object. Created with the New-Shoretel_Session function. This may also be piped to the function.
  #>
    param 
    (      
        [int] $agentId, 

        [Parameter(ValueFromPipeline=$true)]
        [object] $session,

        [string] $shoretelServer,
        [bool] $windowsAuth,
        [string] $user,
        [string] $pass
    )
    if($shoretelServer -ne ""){$session = New-ShoreTel_Session -shoretelServer $shoretelServer -windowsAuth $windowsAuth -user $user -pass $pass}
    elseif($session -eq $null){Write-Error "Session object must be provided"; return}
    if($session.LoggedIn -eq $false -or $session.Authenticated -eq $false){Write-Error "Session must be logged in and authenticated"; return}

    $postData = New-ShoreTel_ExecuteMessage -sequenceId (New-SequenceId $session) -topic "workgroup" -message "login"
    if($agentId) {$postData."agent-id" = $agentId}
    return Send-ShoreTel_ExecuteMessage  -postData $postData -session $session -verbose $PSBoundParameters['Verbose']
}

function Set-ShoreTel_WorkgroupLogout
{
  <#
  .SYNOPSIS
  Logout of ShoreTel workgroups
  .DESCRIPTION
  Logout the current extension of ShoreTel workgroups
  .EXAMPLE
  Set-ShoreTel_WorkgroupLogout -session $session
  .EXAMPLE
  $session | Set-ShoreTel_WorkgroupLogout
  .PARAMETER shoretelServer
  The ip address or hostname of the windows server. This parameter must be provided if an existing session has not be established.
  .PARAMETER windowsAuth
  Controls whether or not to authenticate using your windows credentials. This parameter must be provided if an existing session has not be established.
  .PARAMETER user
  User name to authenticate with. This parameter should only be provided and is mandatory if windows authentication is disabled.
  .PARAMETER pass
  Password to authenticate with. This parameter should only be provided and is mandatory if windows authentication is disabled.
  .PARAMETER session
  Shoretel custom Session object. Created with the New-Shoretel_Session function. This may also be piped to the function.
  #>
    param 
    (       
        [int] $agentId, 
		
        [Parameter(ValueFromPipeline=$true)]
        [object] $session,

        [string] $shoretelServer,
        [bool] $windowsAuth,
        [string] $user,
        [string] $pass
    )
    if($shoretelServer -ne ""){$session = New-ShoreTel_Session -shoretelServer $shoretelServer -windowsAuth $windowsAuth -user $user -pass $pass}
    elseif($session -eq $null){Write-Error "Session object must be provided"; return}
    if($session.LoggedIn -eq $false -or $session.Authenticated -eq $false){Write-Error "Session must be logged in and authenticated"; return}

    $postData = New-ShoreTel_ExecuteMessage -sequenceId (New-SequenceId $session) -topic "workgroup" -message "logout"
    if($agentId) {$postData."agent-id" = $agentId}
    return Send-ShoreTel_ExecuteMessage  -postData $postData -session $session -verbose $PSBoundParameters['Verbose']
}

function Set-ShoreTel_WorkgroupEnterWrap
{
  <#
  .SYNOPSIS
  Set agent to wrap state in ShoreTel workgroups
  .DESCRIPTION
  Set the current agent a wrap state in ShoreTel workgroups
  .EXAMPLE
  Set-ShoreTel_WorkgroupEnterWrap -session $session
  .PARAMETER shoretelServer
  The ip address or hostname of the windows server. This parameter must be provided if an existing session has not be established.
  .PARAMETER windowsAuth
  Controls whether or not to authenticate using your windows credentials. This parameter must be provided if an existing session has not be established.
  .PARAMETER user
  User name to authenticate with. This parameter should only be provided and is mandatory if windows authentication is disabled.
  .PARAMETER pass
  Password to authenticate with. This parameter should only be provided and is mandatory if windows authentication is disabled.
  .EXAMPLE
  $session | Set-ShoreTel_WorkgroupEnterWrap
  .PARAMETER session
  Shoretel custom Session object. Created with the New-Shoretel_Session function. This may also be piped to the function.
  #>
    param 
    (      
        [int] $agentId, 
		
        [Parameter(ValueFromPipeline=$true)]
        [object] $session,

        [string] $shoretelServer,
        [bool] $windowsAuth,
        [string] $user,
        [string] $pass
    )
    if($shoretelServer -ne ""){$session = New-ShoreTel_Session -shoretelServer $shoretelServer -windowsAuth $windowsAuth -user $user -pass $pass}
    elseif($session -eq $null){Write-Error "Session object must be provided"; return}
    if($session.LoggedIn -eq $false -or $session.Authenticated -eq $false){Write-Error "Session must be logged in and authenticated"; return}

    $postData = New-ShoreTel_ExecuteMessage -sequenceId (New-SequenceId $session) -topic "workgroup" -message "enter-wrap-up"
    if($agentId) {$postData."agent-id" = $agentId}
    return Send-ShoreTel_ExecuteMessage  -postData $postData -session $session -verbose $PSBoundParameters['Verbose']
}

function Get-ShoreTel_CallHistory
{
  <#
  .SYNOPSIS
  Gets the ShoreTel call history for the logged in user.
  .DESCRIPTION
  Gets the first 50 history records for the logged in user.
  Use the offset to iterate through the pages.
  .EXAMPLE
  Get-ShoreTel_CallHistory -session $session
  .EXAMPLE
  $session | Get-ShoreTel_CallHistory -Offset 50
  .PARAMETER PageSize
  Record page size, maximum 50.
  .PARAMETER Offset
  Starting offset, expressed as a number.
  .PARAMETER shoretelServer
  The ip address or hostname of the windows server. This parameter must be provided if an existing session has not be established.
  .PARAMETER windowsAuth
  Controls whether or not to authenticate using your windows credentials. This parameter must be provided if an existing session has not be established.
  .PARAMETER user
  User name to authenticate with. This parameter should only be provided and is mandatory if windows authentication is disabled.
  .PARAMETER pass
  Password to authenticate with. This parameter should only be provided and is mandatory if windows authentication is disabled.
  .PARAMETER session
  Shoretel custom Session object. Created with the New-Shoretel_Session function. This may also be piped to the function.
  #>
    param
    (
        [int] $PageSize = 50,
        [int] $Offset = 0,
        [Parameter(ValueFromPipeline=$true)] 
        [object] $session,

        [string] $shoretelServer,
        [bool] $windowsAuth,
        [string] $user,
        [string] $pass
    )
    if($shoretelServer -ne ""){$session = New-ShoreTel_Session -shoretelServer $shoretelServer -windowsAuth $windowsAuth -user $user -pass $pass}
    elseif($session -eq $null){Write-Error "Session object must be provided"; return}
    if($session.LoggedIn -eq $false -or $session.Authenticated -eq $false){Write-Error "Session must be logged in and authenticated"; return}

    $postData = New-ShoreTel_ExecuteMessage -seq (New-SequenceId $session) -topic "history" -message "get-page-view"
    $postData.recid = 99
    $postData."sort-reverse" = 0
    $postData."sort-by" = 0
    $postData."send-events" = 1
    $postData.count = $PageSize
    $postData.offset = $Offset
    $postData."sort-order" = @(4,1,3,2,5)
    $response = Send-ShoreTel_ExecuteMessage  -postData $postData -session $session -returnResults

    if($response.records)
    {
        $records = Convert-ShoreTel_Array -array $response.records
        return @{records=$records; total=$response."total-count"; offset=$response.offset}
    }
    return $false
}

function Convert-FromJumbleHex
{
    param
    (
        $jumbleHex
    )

    $result = ""

    $arr = @()
    for($i = 0;$i -lt $jumbleHex.Length;$i+=2)
    {
        $arr += ([convert]::toint16($jumbleHex.Substring($i,2),16))
    }
    
    $bKey = $arr[1] -band 0x7f

    for($i = 2;$i -lt $arr.Length;$i++)
    {
        $result += [char]($arr[$i] - $bKey)
    }
    
    return $result
}

function Convert-ToJumbleHex
{
    param
    (
        $text
    )
    # Add Char 127 as the first character of the jumble (Marker character)
    $jumble = ([char] 127)
    $bKey = 0

    # Calculate bKey
    for($i = 0; $i -lt $text.length; $i++)
    {
        $bKey = $bKey -bxor ([byte]$text[$i])
    }
    
    if($bKey -eq 0)
    {
        $bKey = 0x55
    }

    $bKey = $bKey -band 0x7f

    # Add bKey (2nd character of jumble)
    $jumble += [char]$bKey

    # Ascii Shift based on bKey
    for($i = 0; $i -lt $text.length; $i++)
    {
        $jumble += [char](([byte] $text[$i]) + $bKey)
    }

    # Convert to Hex
    for($i = 0;$i -lt $jumble.Length;$i++)
    {
        $jumbleHex += [convert]::tostring([byte][char]$jumble[$i], 16).PadLeft(2, "0").ToUpper()
    }
    return $jumbleHex
}

function Convert-FromUnixdate ($UnixDate) {
   [timezone]::CurrentTimeZone.ToLocalTime(([datetime]'1/1/1970').AddMilliseconds($UnixDate))
}