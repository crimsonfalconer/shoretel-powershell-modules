param
(
    $state = (Read-Host "`r`n1. Login `r`n2. Logout `r`n3. Wrap-up `r`n"),
    $adauth = $true,
    $user = "shoreteluser",
    $pass = "shoretelpass",
    $server = ""
)

$Error.Clear()

# Get the current script path
function Get-ScriptPath 
{
    if ($PSScriptRoot) { return $PSScriptRoot } 
    elseif ($psISE) { return split-path -parent $psISE.CurrentFile.Fullpath }
    else { return Split-Path ((Get-Variable MyInvocation -Scope 1).Value).MyCommand.Path }
}
$path = Get-ScriptPath

# if required add it to the module load path to environment variable
if (!($env:PSModulePath -Split ";" -like $path)) { $env:PSModulePath += ";$path" }

# Import the shoretel module
Import-Module ShoreTel -Force -Verbose

if($server -eq "")
{
    $server = Read-Host "Enter shoretel server computer name or ip address"
}

if($state -eq "1")
{
    Set-ShoreTel_WorkgroupLogin -shoretelServer $server -windowsAuth $adauth -user $user -pass $pass
}
elseif($state -eq "2")
{
    Set-ShoreTel_WorkgroupLogout -shoretelServer $server -windowsAuth $adauth -user $user -pass $pass
}
elseif($state -eq "3")
{
    Set-ShoreTel_WorkgroupEnterWrap -shoretelServer $server -windowsAuth $adauth -user $user -pass $pass
}
